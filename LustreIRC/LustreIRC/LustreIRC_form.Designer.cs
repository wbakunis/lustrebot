﻿namespace LustreIRC {
    partial class LustreIrc_form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.irc_TextBox = new System.Windows.Forms.RichTextBox();
            this.chattersTextBox = new System.Windows.Forms.TextBox();
            this.inputTextBox = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // irc_TextBox
            // 
            this.irc_TextBox.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.irc_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.irc_TextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.irc_TextBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.irc_TextBox.ForeColor = System.Drawing.Color.Lime;
            this.irc_TextBox.Location = new System.Drawing.Point(0, 24);
            this.irc_TextBox.Name = "irc_TextBox";
            this.irc_TextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.irc_TextBox.ShortcutsEnabled = false;
            this.irc_TextBox.Size = new System.Drawing.Size(489, 399);
            this.irc_TextBox.TabIndex = 0;
            this.irc_TextBox.TabStop = false;
            this.irc_TextBox.Text = "";
            // 
            // chattersTextBox
            // 
            this.chattersTextBox.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.chattersTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.chattersTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.chattersTextBox.Dock = System.Windows.Forms.DockStyle.Right;
            this.chattersTextBox.ForeColor = System.Drawing.Color.Lime;
            this.chattersTextBox.Location = new System.Drawing.Point(494, 24);
            this.chattersTextBox.Multiline = true;
            this.chattersTextBox.Name = "chattersTextBox";
            this.chattersTextBox.ReadOnly = true;
            this.chattersTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.chattersTextBox.ShortcutsEnabled = false;
            this.chattersTextBox.Size = new System.Drawing.Size(210, 399);
            this.chattersTextBox.TabIndex = 0;
            this.chattersTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // inputTextBox
            // 
            this.inputTextBox.AcceptsReturn = true;
            this.inputTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.inputTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.inputTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputTextBox.Location = new System.Drawing.Point(0, 423);
            this.inputTextBox.Name = "inputTextBox";
            this.inputTextBox.Size = new System.Drawing.Size(704, 19);
            this.inputTextBox.TabIndex = 2;
            this.inputTextBox.WordWrap = false;
            this.inputTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inputTextBox_KeyDown);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(704, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // startToolStripMenuItem
            // 
            this.startToolStripMenuItem.Name = "startToolStripMenuItem";
            this.startToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.startToolStripMenuItem.Text = "Start";
            this.startToolStripMenuItem.Click += new System.EventHandler(this.startToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // LustreIrc_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 442);
            this.Controls.Add(this.chattersTextBox);
            this.Controls.Add(this.irc_TextBox);
            this.Controls.Add(this.inputTextBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(720, 480);
            this.MinimumSize = new System.Drawing.Size(720, 480);
            this.Name = "LustreIrc_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LustreIRC";
            this.Load += new System.EventHandler(this.LustreIrc_form_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.RichTextBox irc_TextBox;
          public System.Windows.Forms.TextBox chattersTextBox;
        private System.Windows.Forms.TextBox inputTextBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}


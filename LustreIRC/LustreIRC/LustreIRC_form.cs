﻿using LustreIRC.Scripts;
using LustreIRC.Scripts.Commands;
using LustreIRC.Scripts.JSON_Handlers;
using LustreIRC.Scripts.Mechanics;
using System;
using System.IO;
using System.Windows.Forms;

namespace LustreIRC
{
    public partial class LustreIrc_form : Form, IDisposable
    {
        public int ChatLineCount;

        public string Chatters;

        //public ChannelMisc_JSON cm;
        public string fileLoc = "E:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/private/LustreReader/LustreReader_oauth.txt";

        public Followers_JSON fj;
        public ircClient irc;

        public bool isBotIdling;

        public bool isBotRunning;

        public bool isStartingBot;

        //public Chatters_JSON cj;
        public JSON_timer jt;

        public ParseViewers pv;

        //public ModCommands mc;
        public string reader;

        public string UserMessage;
        public int ViewerCount;

        public LustreIrc_form()
        {
            InitializeComponent();
            isBotIdling = true;
            irc_TextBox.Text = "Bot idling" + "\r\n";
            //Console.Title = Application.ProductName + " | " + Application.ProductVersion;
            //Console.ForegroundColor = ConsoleColor.Green;
            //if (StartBot)
            //{
            //    AuthorizeForm();
            //    jt = new JSON_timer();
            //    pv = new ParseViewers();
            //    pv.onParseViewersTimer();
            //    jt.onStartJSONTimers();
            //    ListViewers();
            //    onBotConnect();
            //}
        }

        private void AuthorizeForm()
        {
            try
            {
                using (StreamReader sr = new StreamReader(fileLoc))
                {
                    reader = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[AUTHORIZATION] oAuth File not found");
                Console.WriteLine(e);
            }
        }

        private void ChatNewLine(string newLine)
        {
            irc_TextBox.Text += newLine + "\r\n";
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void inputTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ChatNewLine(inputTextBox.Text);
            }
        }

        private void ListViewers()
        {
            //Console.WriteLine("VIEWERS:" + pv.ViewerNames);
            chattersTextBox.Text = string.Join(string.Empty, pv.ViewerNames);
        }

        private void LustreIrc_form_Load(object sender, EventArgs e)
        {
        }

        private void onBotConnect()
        {
            irc = new ircClient("Lustrebot", reader);
            irc.onJoinRoom("#lustredust");

            while (true)
            {
                isBotIdling = false;
                isBotRunning = true;
                int bufferSize = irc.tcpClient.ReceiveBufferSize;
                int aData = irc.tcpClient.Available;
                UserMessage = irc.readMessage();
                string[] umSplit = UserMessage.Split(';');
                int umLength = umSplit.Length;
                Console.WriteLine(umLength);
                foreach (String split in umSplit)
                {
                    Console.WriteLine(split);
                }
                string Color = umSplit[0];
                string Msg;
                //Console.WriteLine(bufferSize);
                //Console.WriteLine(aData);
                if (UserMessage.Contains("!lbconsole"))
                {
                    irc.sendMessage("Console is running. | version: " + Application.ProductVersion);
                    //irc_TextBox.Text = "LustreReader is active" + "\n";
                }
                if (UserMessage.Contains("!Clear"))
                {
                    irc.ClearChat();
                }
                //if (message.Contains("!tr")) {
                //    irc.sendMessage("LustreReader is active");
                //    //irc_TextBox.Text = "LustreReader is active" + "\n";
                //}
                //if (message.Contains("!tr disable")) {
                //    Application.Exit();
                //    //irc_TextBox.Text = "LustreReader is active" + "\n";
                //}
            }
        }

        private void onStartBot()
        {
            if (isStartingBot)
            {
                AuthorizeForm();
                jt = new JSON_timer();
                pv = new ParseViewers();
                pv.onParseViewersTimer();
                jt.onStartJSONTimers();
                ListViewers();
                onBotConnect();
            }
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isStartingBot = true;
            onStartBot();
        }
    }
}
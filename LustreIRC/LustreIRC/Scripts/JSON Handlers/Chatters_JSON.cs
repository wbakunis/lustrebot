﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Text;
using System.Timers;

namespace LustreIRC.Scripts.JSON_Handlers
{
    public class Chatters_JSON
    {
        private string chatterFile_loc = "E:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/Lustrebot/LustreIRC/LustreIRC/JSON Files/Chatters.json";
        private string ChattersJSON_url = "https://tmi.twitch.tv/group/user/lustredust/chatters";
        //private System.Timers.Timer chattersTimer;
        //private int cTimer_time = 60000;//1 minute | 1000ms = 1second

        //public void onRetrieveChatterTimer() {
        //    chattersTimer = new System.Timers.Timer(cTimer_time);
        //    chattersTimer.Elapsed += onRetrieveChattersInfo;
        //    Console.WriteLine("Chatter Timers starting! | " + DateTime.Now.ToLongDateString());
        //    chattersTimer.AutoReset = true;
        //    chattersTimer.Enabled = true;
        //}

        public void onRetrieveChattersInfo(Object o, ElapsedEventArgs eea)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    Console.WriteLine("[UPDATING CHATTERS.JSON FILE!] | " + DateTime.UtcNow);
                    UnicodeEncoding byter = new UnicodeEncoding();
                    var retrievedJSON = wc.DownloadString(ChattersJSON_url);
                    string formatJSON = JValue.Parse(retrievedJSON).ToString(Formatting.Indented);
                    System.IO.File.WriteAllText(chatterFile_loc, formatJSON);
                    //Properties.Resources.Chatters;
                    //Console.Write(json);
                    // Now parse with JSON.Net
                }
            }
            catch (WebException we)
            {
                Console.WriteLine(we);
            }
        }
    }
}
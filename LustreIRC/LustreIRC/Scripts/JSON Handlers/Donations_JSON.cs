﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Text;
using System.Timers;

namespace LustreIRC.Scripts.JSON_Handlers
{
    public class Donations_JSON
    {
        public string DonationChecker_loc = "E:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/Lustrebot/LustreIRC/LustreIRC/JSON Files/DonationNew.json";
        public string DonationChecker_url = "E:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/private/streamtip_checkerURL.txt";
        public string DonationCurrentGoal_loc = "E:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/Lustrebot/LustreIRC/LustreIRC/JSON Files/DonationCurrentGoal.json";
        public string DonationCurrentGoal_url = "https://streamtip.com/api/public/twitch/lustredust/goal";
        public string DonationTipsList_loc = "E:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/Lustrebot/LustreIRC/LustreIRC/JSON Files/DonationTopTips.json";
        public string DonationTipsList_url = "E:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/private/streamtip_tipslistURL.txt";
        public string DonationTop_loc = "E:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/Lustrebot/LustreIRC/LustreIRC/JSON Files/DonationTopTips.json";
        public string DonationTop_url = "E:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/private/streamtip_toptipsURL.txt";

        public void onRetrieveDonationCheckerInfo(Object o, ElapsedEventArgs eea)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    Console.WriteLine("[UPDATING DONATIONNEW.JSON FILE!] | " + DateTime.UtcNow);
                    UnicodeEncoding byter = new UnicodeEncoding();
                    var retrievedJSON = wc.DownloadString(DonationChecker_url);
                    string formatJSON = JValue.Parse(retrievedJSON).ToString(Formatting.Indented);
                    System.IO.File.WriteAllText(DonationChecker_loc, formatJSON);
                    //Properties.Resources.Chatters;
                    //Console.Write(json);
                    // Now parse with JSON.Net
                }
            }
            catch (WebException we)
            {
                Console.WriteLine(we);
            }
        }

        public void onRetrieveDonationCurrentGoalInfo(Object o, ElapsedEventArgs eea)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    Console.WriteLine("[UPDATING DONATIONCURRENTGOAL.JSON FILE!] | " + DateTime.UtcNow);
                    UnicodeEncoding byter = new UnicodeEncoding();
                    var retrievedJSON = wc.DownloadString(DonationCurrentGoal_url);
                    string formatJSON = JValue.Parse(retrievedJSON).ToString(Formatting.Indented);
                    System.IO.File.WriteAllText(DonationCurrentGoal_loc, formatJSON);
                    //Properties.Resources.Chatters;
                    //Console.Write(json);
                    // Now parse with JSON.Net
                }
            }
            catch (WebException we)
            {
                Console.WriteLine(we);
            }
        }

        public void onRetrieveDonationTipsListInfo(Object o, ElapsedEventArgs eea)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    Console.WriteLine("[UPDATING DONATIONTIPSLIST.JSON FILE!] | " + DateTime.UtcNow);
                    UnicodeEncoding byter = new UnicodeEncoding();
                    var retrievedJSON = wc.DownloadString(DonationTipsList_url);
                    string formatJSON = JValue.Parse(retrievedJSON).ToString(Formatting.Indented);
                    System.IO.File.WriteAllText(DonationTipsList_loc, formatJSON);
                    //Properties.Resources.Chatters;
                    //Console.Write(json);
                    // Now parse with JSON.Net
                }
            }
            catch (WebException we)
            {
                Console.WriteLine(we);
            }
        }

        public void onRetrieveDonationTopInfo(Object o, ElapsedEventArgs eea)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    Console.WriteLine("[UPDATING DONATIONTOPTIPS.JSON FILE!] | " + DateTime.UtcNow);
                    UnicodeEncoding byter = new UnicodeEncoding();
                    var retrievedJSON = wc.DownloadString(DonationTop_url);
                    string formatJSON = JValue.Parse(retrievedJSON).ToString(Formatting.Indented);
                    System.IO.File.WriteAllText(DonationTop_loc, formatJSON);
                    //Properties.Resources.Chatters;
                    //Console.Write(json);
                    // Now parse with JSON.Net
                }
            }
            catch (WebException we)
            {
                Console.WriteLine(we);
            }
        }
    }
}
﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Text;
using System.Timers;

namespace LustreIRC.Scripts.JSON_Handlers
{
    public class ChannelMisc_JSON
    {
        public string LastChannelFeed_loc = "E:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/Lustrebot/LustreIRC/LustreIRC/JSON Files/LastChannelFeed.json";
        public string LastChannelFeed_url = "https://api.twitch.tv/kraken/feed/lustredust/posts?limit=1";
        public string LastChannelHighLight_loc = "E:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/Lustrebot/LustreIRC/LustreIRC/JSON Files/LastChannelHighlight.json";
        public string LastChannelHighLight_url = "https://api.twitch.tv/kraken/channels/lustredust/videos?limit=1";
        //private System.Timers.Timer LCFchannelTimer;
        //private System.Timers.Timer LCHchannelTimer;
        //private int cTimer_time = 60000;//1 minute | 1000ms = 1second

        //public void onRetrieveLFTimer() {
        //    LCFchannelTimer = new System.Timers.Timer(cTimer_time);
        //    LCFchannelTimer.Elapsed += onRetrieveLastFeedInfo;
        //    Console.WriteLine("Last Feed Timers starting! | " + DateTime.Now.ToLongDateString());
        //    LCFchannelTimer.AutoReset = true;
        //    LCFchannelTimer.Enabled = true;
        //}

        //public void onRetrieveLHTimer() {
        //    LCHchannelTimer = new System.Timers.Timer(cTimer_time);
        //    LCHchannelTimer.Elapsed += onRetrieveLastHighLightInfo;
        //    Console.WriteLine("Last Channel Highlight Timers starting! | " + DateTime.Now.ToLongDateString());
        //    LCHchannelTimer.AutoReset = true;
        //    LCHchannelTimer.Enabled = true;
        //}

        public void onRetrieveLastFeedInfo(Object o, ElapsedEventArgs eea)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    Console.WriteLine("[UPDATING LASTCHANNELFEED.JSON FILE!] | " + DateTime.UtcNow);
                    UnicodeEncoding byter = new UnicodeEncoding();
                    var retrievedJSON = wc.DownloadString(LastChannelFeed_url);
                    string formatJSON = JValue.Parse(retrievedJSON).ToString(Formatting.Indented);
                    System.IO.File.WriteAllText(LastChannelFeed_loc, formatJSON);
                    //Properties.Resources.Chatters;
                    //Console.Write(json);
                    // Now parse with JSON.Net
                }
            }
            catch (WebException we)
            {
                Console.WriteLine(we);
            }
        }

        public void onRetrieveLastHighLightInfo(Object o, ElapsedEventArgs eea)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    Console.WriteLine("[UPDATING LASTCHANNELHIGHLIGHT.JSON FILE!] | " + DateTime.UtcNow);
                    UnicodeEncoding byter = new UnicodeEncoding();
                    var retrievedJSON = wc.DownloadString(LastChannelHighLight_url);
                    string formatJSON = JValue.Parse(retrievedJSON).ToString(Formatting.Indented);
                    System.IO.File.WriteAllText(LastChannelHighLight_loc, formatJSON);
                    //Properties.Resources.Chatters;
                    //Console.Write(json);
                    // Now parse with JSON.Net
                }
            }
            catch (WebException we)
            {
                Console.WriteLine(we);
            }
        }
    }
}
﻿using System;

namespace LustreIRC.Scripts.Mechanics
{
    internal class Permissions
    {
        public bool isAdmin()
        {
            if ("" == "")
            {
                Console.WriteLine("is a site admin");
                //return true;
            }
            Console.WriteLine("Not a site admin");
            return false;
        }

        public bool isBroadCaster()
        {
            if ("" == "")
            {
                Console.WriteLine("is the broadcaster");
                //return true;
            }
            Console.WriteLine("Not the broadcaster");
            return false;
        }

        public bool isGlobalMod()
        {
            if ("" == "")
            {
                Console.WriteLine("is a global moderator");
                //return true;
            }
            Console.WriteLine("Not a global moderator");
            return false;
        }

        public bool isMod()
        {
            if ("" == "")
            {
                Console.WriteLine("is a mod");
                //return true;
            }
            Console.WriteLine("Not a mod");
            return false;
        }

        public bool isSub()
        {
            if ("" == "")
            {
                Console.WriteLine("is a Subscriber");
                //return true;
            }
            Console.WriteLine("Not a Subscriber");
            return false;
        }

        public bool isTurbo()
        {
            if ("" == "")
            {
                Console.WriteLine("is a Turbo Member");
                //return true;
            }
            Console.WriteLine("Not a Turbo Member");
            return false;
        }
    }
}
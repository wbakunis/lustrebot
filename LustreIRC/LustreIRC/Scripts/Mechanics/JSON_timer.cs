﻿using LustreIRC.Scripts.JSON_Handlers;
using System;

namespace LustreIRC.Scripts.Mechanics
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable")]
    public class JSON_timer
    {
        private System.Timers.Timer CentralJSON_timer;
        private int centralTimer_time = 60000;//1 minute | 1000ms = 1second
        private Chatters_JSON cj = new Chatters_JSON();
        private ChannelMisc_JSON cmj = new ChannelMisc_JSON();
        private Donations_JSON dj = new Donations_JSON();
        private Followers_JSON fj = new Followers_JSON();
        private WunderGround_JSON wgj = new WunderGround_JSON();

        public void onStartJSONTimers()
        {
            CentralJSON_timer = new System.Timers.Timer(centralTimer_time);
            CentralJSON_timer.Elapsed += cmj.onRetrieveLastFeedInfo;
            CentralJSON_timer.Elapsed += cmj.onRetrieveLastHighLightInfo;
            //CentralJSON_timer.Elapsed += wgj.onRetrieveWunderGroundInfo;
            CentralJSON_timer.Elapsed += fj.onRetrieveLastFollowerInfo;
            CentralJSON_timer.Elapsed += cj.onRetrieveChattersInfo;
            CentralJSON_timer.Elapsed += dj.onRetrieveDonationTopInfo;
            CentralJSON_timer.Elapsed += dj.onRetrieveDonationTipsListInfo;
            CentralJSON_timer.Elapsed += dj.onRetrieveDonationCheckerInfo;
            CentralJSON_timer.Elapsed += dj.onRetrieveDonationCurrentGoalInfo;
            Console.WriteLine("[JSON TIMERS STARTING!] | " + DateTime.UtcNow);
            CentralJSON_timer.AutoReset = true;
            CentralJSON_timer.Enabled = true;
        }

        private void Dispose()
        {
        }
    }
}
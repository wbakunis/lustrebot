﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.IO;
using System.Timers;

namespace LustreIRC.Scripts.Mechanics
{
    public class ParseViewers
    {
        public ArrayList ViewerNames;
        private string ChatterJSON_loc = "E:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/Lustrebot/LustreIRC/LustreIRC/JSON Files/Chatters.json";

        //private StreamReader vSR;
        //private JsonTextReader vJTR;
        private System.Timers.Timer ParseViewers_timer;

        private int ViewersTimer_time = 60000;//1 minute | 1000ms = 1second

        public void onParseViewers(Object o, ElapsedEventArgs eea)
        {
            try
            {
                JObject JVO = JObject.Parse(File.ReadAllText(ChatterJSON_loc));
                JArray viewers = (JArray)JVO["chatters"]["viewers"];
                Console.WriteLine("Adding names to list");
                ViewerNames = new ArrayList();
                foreach (string jov in viewers)
                {
                    if (jov.Length >= 1)
                    {
                        ViewerNames.Add((JValue)viewers.ToString());
                    }
                    else
                    {
                        ViewerNames.Add("No viewers");
                    }
                    Console.WriteLine(jov);
                }
            }
            catch (Exception JRE)
            {
                Console.WriteLine(JRE);
            }
        }

        public void onParseViewersTimer()
        {
            ParseViewers_timer = new System.Timers.Timer(ViewersTimer_time);
            ParseViewers_timer.Elapsed += onParseViewers;
            Console.WriteLine("[VIEWERPARSE TIMERS STARTING!] | " + DateTime.UtcNow);
            ParseViewers_timer.AutoReset = true;
            ParseViewers_timer.Enabled = true;
        }
    }
}
;; GLOBAL VARIABLES

on *:join:#: {
  if ($nick == lustrebot) {
    set %gloperm You lack permission to use this command.
    set %gloLostFile File appears to be missing!
  }
}

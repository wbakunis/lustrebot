alias LoadAll {
  load -rs twitterManager.mrc
  load -rs wundergroundAPi.mrc
  load -rs devscript.mrc
  load -rs donationCommands.mrc
  load -rs follower.mrc
  load -rs giveaway.mrc
  load -rs globalvariables.mrc
  load -rs jar_command.mrc
  load -rs jokeCommands.mrc
  load -rs json_alias.mrc
  load -rs jsonprimer.mrc
  load -rs LustreBot.mrc
  load -rs mechanics.mrc
  load -rs permissions.mrc
  load -rs quote_commands.mrc
  load -rs randcommands.mrc
  load -rs songManager.mrc
  load -rs suggestart_commands.mrc
  load -rs timeago.mrc
  load -rs timers.mrc
  load -rs toastManager.mrc
}

;;;;;;;;;;;;;;;

on *:text:!browse*:#: {
  browseStore
}

alias browseStore {
  var %u = http://lustredust.storenvy.com/products.json
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %u
  var %id = $JSON(%v,stream,_id)
  var %status = $JSON(%v,status)
  msg $chan %status 
  :end
  JSONClose %v
}

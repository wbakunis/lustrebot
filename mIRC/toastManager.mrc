; THIS CODE IS A MODERATELY MODIFIED FORM OF JALLY511'S "CARROT" POINT SYSTEM. CODE WAS FOUND ON HAWKEE.COM AND HAS NO FORM OF LICENSE.
; http://hawkee.com/snippet/16232/

on *:text:!admincheck*:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
    if ($0 < 2) {
      msg $chan Incorrect usage: Use !admincheck <user>
      return
    }
    msg $chan $2 currently has $readini(Toasts.ini,$+($chan,., $2),toasts) slices of toast.
  }
}

on *:text:!toastInfo:#: {
  msg $chan $nick , your current rank is $flooredRank $+ .
}

on *:text:!toastInfow:#: {
  msg $chan /w $nick $nick , your current rank is $flooredRank $+ .
}

on *:text:!songcharge*:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
    if ($0 < 3) {
      msg $chan Incorrect usage: Use !songcharge <on|off> <price>
      return
    }
    set %songPrice $3
    elseif (%songchargeon.songchargeoff == on) {
      msg $chan Art charge is already on with a set price of %artPrice $+ !
      return
    }
    elseif ($2 == on) {
      set %songchargeon.songchargeoff on 
      msg $chan $nick Song suggestions cost $3 toasts now.
    }
    elseif ($2 == off) {
      unset %songchargeon.songchargeoff  
      set %songPrice 0
      msg $chan $nick Song suggestions are now free.
    }
  }
}

on *:text:!artcharge*:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
    if ($0 < 3) {
      msg $chan Incorrect usage: Use !artcharge <on|off> <price>
      return
    }
    set %artPrice $3
    elseif (%artchargeon.artchargeoff == on) {
      msg $chan Art charge is already on with a set price of %artPrice $+ !
      return
    }
    elseif ($2 == on) {
      set %artchargeon.artchargeoff on 
      msg $chan $nick art suggestions cost $3 toasts now.
    }
    elseif ($2 == off) {
      unset %artchargeon.artchargeoff  
      set %artPrice 0
      msg $chan $nick art suggestions are now free.
    }
  }
}

on *:text:!toast4all:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
    var %userlist = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\userlist.txt
    if ($exists(%userlist)) {
      var %namelist_length = $lines(%userlist)
      var %t4all = 0
      ;starting at 0 = start of array
      while (%t4all <= %namelist_length) {
        var %line = $read(%userlist)
        writeini -n Toasts.ini $+(#,.,%line) Toasts $calc($readini(Toasts.ini,$+(#,.,%line),Toasts) + 10)
        inc %t4all
        /echo -a %t4all
      }
      if (%t4all == %namelist_length) {
        msg $chan /me TOAST FOR ALL!!!!
        msg $chan /me **TOAST THROWING INTENSIFIES**
        halt
      }
    }
    else msg $chan %gloLostFile
  }
  else msg $chan %gloperm
}

alias flooredRank {
  var %toastCalc = $readini(Toasts.ini,$+($chan,., $nick),toasts)
  var %calcModifier = 100
  var %calcRank = $calc(%toastcalc / %calcModifier)
  var %calcFloor = $Floor(%calcRank)
  return %calcFloor
}

alias toastCalcUp {
  var %toastCalcUp = $readini(Toasts.ini,$+($chan,., $nick),toasts)
  var %calcUpModifier = 100
  var %calcUp = $calc(%toastCalcUp * %calcUpModifier)
  return %calcUp
}

alias rawRank {
  var %toastCalc = $readini(Toasts.ini,$+($chan,., $nick),toasts)
  var %calcModifier = 100
  var %calcRank = $calc(%toastcalc / %calcModifier)
  return %calcRank
}

alias -l addToasts {
  if ($1 !isnum) { 
    echo 2 -st $1 is not a number. It needs to be a number. | halt 
  }
  var %topic $+($chan,.,$nick)
  var %Toasts $calc($readini(Toasts.ini,%topic,Toasts) + $1)
  writeini -n Toasts.ini %topic Toasts %Toasts
  return %Toasts
}
alias -l feedToasts {
  if ($4 !isnum) {
    echo 2 -st $4 is not a number. It needs to be a number. | halt 
  }
  var %topic $+($chan,.,$nick)
  var %Toasts $calc($readini(Toasts.ini,%topic,Toasts) + $4)
  var %remove = %user - $4
  writeini -n Toasts.ini %user Toasts %Toasts
  writeini -n Toasts.ini %topic Toasts %remove
  return %Toasts
}
alias -l lookUpToasts {
  var %topic $+($chan,.,$nick)
  var %Toasts $readini(Toasts.ini,%topic,Toasts)
  return %Toasts
}
alias doaddToasts {
  if ($3 !isnum) { 
    echo 2 -st $3 is not a number. It needs to be a number. | halt 
  }
  var %topic $+($1,.,$2)
  var %Toasts $calc($readini(Toasts.ini,%topic,Toasts) + $3)
  writeini -n Toasts.ini %topic Toasts %Toasts
  echo -a Added Toasts for %topic
}
alias doremToasts {
  var %topic $+($1,.,$2)
  remini -n Toasts.ini %topic Toasts
  echo -a Removed Toasts for %topic
}

on *:text:!checktoast:#: {
  var %toastslices = $readini(Toasts.ini,$+($chan,., $nick),toasts) 
  if (%toastslices != $null) { 
    if (%toastslices !< 12) {
      var %loaf = %toastslices / 12
      msg $chan $nick you have %toastslices slices or $round(%loaf,n) loaves.
    } 
    else msg $chan $nick you have %toastslices slices or $round(%loaf,n) loaves.
  }
  else msg $chan $nick you have no toast!
  return
}

on *:text:!checktoastw:#: {
  var %toastslices = $readini(Toasts.ini,$+($chan,., $nick),toasts) 
  if (%toastslices != $null) { 
    if (%toastslices !< 12) {
      var %loaf = %toastslices / 12
      msg $chan /w $nick you have %toastslices slices or $round(%loaf,n) loaves.
    } 
    else msg $chan /w $nick you have %toastslices slices or $round(%loaf,n) loaves.
  }
  else msg $chan /w $nick you have no toast!
  return
} 

on $*:TEXT:/!toast (add|remove)/Si:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
    if ($0 < 3) { 
      msg $chan Incorrect usage: Use !toast <add|remove> <user> [number]
      return
    }
    writeini -n Toasts.ini $+(#,.,$3) Toasts $calc($readini(Toasts.ini,$+(#,.,$3),Toasts) $iif($2 == add,+,-) $iif($4 isnum,$4,1))
    msg $chan /w $3 You now have $readini(Toasts.ini,$+(#,.,$3),Toasts) total toast. 
  }
  else msg $chan %gloperm  
  return
}

on *:text:!givetoast *:#: {
  if ($0 != 3) || ($3 !isnum) {
    msg $chan Incorrect usage !givetoast <user> <amount>
    return
  }
  if ($3 <= 0) {
    msg $chan $nick Why give $2 0 slices of toast?
    return
  }
  if ($readini(Toasts.ini,$+(#,.,$2),Toasts) = $null) {
    msg $chan /w $nick Unable to find that person. Please check spelling
    return
  }
  else {
    if ($readini(Toasts.ini,$+(#,.,$nick),Toasts) >= $3) {
      writeini -n Toasts.ini $2 Toasts $calc($readini(Toasts.ini,$2,$3) + $3)
      writeini -n Toasts.ini $2 Toasts $calc($readini(Toasts.ini,$2,$3) - $3)
      msg $chan $nick has given $3 of their toast to $2, $2 now has $readini(Toasts.ini,$+(#,.,$2),Toasts) toast and $nick now has $readini(Toasts.ini,$+(#,.,$nick),Toasts) $+ .
      return
    }
    elseif ($readini(Toasts.ini,$+(#,.,$nick),Toasts) < $3) {
      msg $chan /w $nick, you do not have that much toast to give!
      return
    }
  }
}

on !*:join:#: {
  ;For every 300 seconds = 5 minutes
  var %timerSeconds = 900
  var %timerBase = 60
  var %timerToasts = %timerSeconds / %timerBase
  ;^^^^^ This converts seconds to minutes ^^^^^^
  $+(.timerToasts.,#,.,$nick) 0 %timerSeconds add.pts $+(#,.,$nick)
  add.pts $+(#,.,$nick)
  if ((%floodjoin) || ($($+(%,floodjoin.,$nick),2))) {
    return 
  }
  set -u10 %floodjoin On
  set -u30 %floodjoin. $+ $nick On
}

ON !*:part:#:$+(.timerToasts.,#,.,$nick) off

alias -l add.pts {
  var %u = https://api.twitch.tv/kraken/streams/lustredust?client_id=h1qvczn5j1ehp69bfl03ymtvrbfurts
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %u
  var %streamStatus = $JSON(%v,stream)
  if (%streamStatus == $null) {
    writeini -n Toasts.ini $1 Toasts $calc($readini(Toasts.ini,$1,Toasts) + 1)
    echo Stream offline. $1 given 1 slice.
  }
  else {
    writeini -n Toasts.ini $1 Toasts $calc($readini(Toasts.ini,$1,Toasts) + 5)
    echo Stream online. $1 given 5 slices.
  }
  ;if ($readini(Toasts.ini,$+(#,.,$nick),Toasts) >= 100 & <= 150) {
  ;  msg $chan $nick is now a known regular! Thanks for hanging around!
  ;}
  :end
  JSONClose %v
}

alias _timeago {
  if (!$1) { return 0 }
  if ($1) && ($1 !isnum) { return 0 }
  if ($prop == all) {
    ; var %s = $nduration($1)
    var %s = $replacex(%s,wks,$chr(32) weeks.,wk,$chr(32) week.)
    var %s = $replacex(%s,days,$chr(32) days.,day,$chr(32) day.)
    if (*min* iswm %s) { var %s = $replacex(%s,hrs,$chr(32) hours.,hr,$chr(32) hour.) }
    if (*min* !iswm %s) { var %s = $replacex(%s,hrs,$chr(32) hours,hr,$chr(32) hour) }
    if (*sec* iswm %s) { var %s = $replacex(%s,mins,$chr(32) minutes.,min,$chr(32) minute.) }
    if (*sec* !iswm %s) { var %s = $replacex(%s,mins,$chr(32) minutes,min,$chr(32) minute) }
    var %s = $replacex(%s,secs,$chr(32) seconds,sec,$chr(32) second)
    var %s = $replace(%s,$chr(46),$chr(44))
    return $iif(%s,%s ago,0)
  }
  if ($1 <= 59) { return $1 $iif($1 == 1,second,seconds) ago }
  if (($1 <= 3599) && ($1 > 59)) { return $floor($calc($1 / 60)) $iif($floor($calc($1 / 60)) == 1,minute,minutes) ago }
  if (($1 <= 86399) && ($1 > 3599)) { return $round($calc($1 / 3600),0) $iif($round($calc($1 / 3600),0) == 1,hour,hours) ago }
  if (($1 <= 2592000) && ($1 > 86399)) { return $floor($calc($1 / 86400)) $iif($floor($calc($1 / 86400)) > 1,days,day) $+ , $round($calc($calc($1 - $calc($floor($calc($1 / 86400))*86400)) / 3600),0) $iif($round($calc($calc($1 - $calc($floor($calc($1 / 86400))*86400)) / 3600),0) > 1,hours,hour) ago }
  if (($1 <= 31540000) && ($1 > 2592000)) { return $floor($calc($1 / 2592000)) $iif($floor($calc($1 / 2592000)) > 1,months,month) $+ , $round($calc($calc($1 - $calc($floor($calc($1 / 2592000))*2592000)) / 86400),0) $iif($round($calc($calc($1 - $calc($floor($calc($1 / 2592000))*2592000)) / 86400),0) > 1,days,day) ago }
  if ($1 > 31104000) { return $floor($calc($1 / 31104000)) $iif($floor($calc($1 / 31104000)) > 1,years,year) $+ , $round($calc($calc($1 - $calc($floor($calc($1 / 31104000))*31104000)) / 2592000),0) $iif($round($calc($calc($1 - $calc($floor($calc($1 / 31104000))*31104000)) / 2592000),0) > 1,months,month) ago }
}

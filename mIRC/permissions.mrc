alias isMod {
  if ($msgtags(user-type).key == mod) {
    return $true
  }
  ; msg $chan Permissions down. Please contact your local service person.[isMOD]

  return $false
}

alias hasModPowers {
  if ($istok(mod global_mod admin staff,$msgtags(user-type).key,32) || $isBroadcaster) {
    return $true
  }
  ; msg $chan Permissions down. Please contact your local service person.[hasMOD]

  return $false
}

alias isSub {
  if ($msgtags(subscriber).key == 1) {
    return $true
  }
  ;  msg $chan Permissions down. Please contact your local service person.[isSub]

  return $false
}

alias isBroadcaster {
  if ($chr(35) $+ $nick == $chan) {
    return $true
  }
  ;  msg $chan Permissions down. Please contact your local service person.[isBroadcaster]

  return $false
}

alias hasTurbo {
  if ($msgtags(turbo).key == 1) {
    return $true
  }
  ;  msg $chan Permissions down. Please contact your local service person.[hasTurbo]
  return $false
}

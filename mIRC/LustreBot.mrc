/*
my name is william. i'm also known as nucloid, chernobyl360, and topgear93. i am the one that put together "lustrebot". a good portion of the code is written by me.
a small portion of the code is found on the internet. the main part found on the internet is the permission system.

the permission system was found on hawkee.com
code references were found on https://en.wikibooks.org/wiki/mirc_scripting

this script is only written to be used on mirc for twitch based chats.
*/

alias timeruptime {
  msg $chan %chanonline
}

alias timersuggest {
  msg $chan if you have an art idea, use .!suggestart <idea> <picture>
}

on *:join:#: {
  if ($nick == $me) {
    ;    msg $chan /w colonelpetrenko lustrebot now enabled & please enable timers.
    msg $chan /color hotpink
    msg $chan /timerUptime 0 3600 $timeruptime
    msg $chan /timerSuggestArt 0 1800 $timersuggest
    msg $chan /timerCurrentPaint 0 600 $CurrentProject
    set %Muson.Musoff on
    set %Arton.Artoff on 
    set %songPrice 0
    set %playerReqRank 1
    ; .timertip 0 15 CheckNewDonations
  }
  if ($nick == colonelpetrenko) {
    ;  msg $chan welcome @colonelpetrenko.
  }
  ; if ($nick == lustredust) {
  ;  if (!%lustredust) {
  ;    set -eu3600 %lustredust 1
  ;   var %ldjoin = $rand(1,5)
  ;   if (%ldjoin == 1) msg $chan praise lianne!
  ;  if (%ldjoin == 2) msg $chan hello ma'am!
  ;  if (%ldjoin == 3) msg $chan lustredust is in the channel!
  ;  if (%ldjoin == 4) msg $chan our lord & savior is in the channel!
  ;  if (%ldjoin == 5) msg $chan i spy @lustredust Kappa !
  ; }
  ; }
  ;  if ($nick == bensonkarlspopovich) {
  ;  if (!%bensonkarlspopovich) {
  ;    set -eu3600 %bensonkarlspopovich 1
  ;     var %bkjoin = $rand(1,5)
  ;    if (%bkjoin == 1) msg $chan Hello sir!
  ;     if (%bkjoin == 2) msg $chan I spy Lustredude
  ;    if (%bkjoin == 3) msg $chan Lustredude is in the channel!
  ;    if (%bkjoin == 4) msg $chan Our mighty speaker has joined!
  ;     if (%bkjoin == 5) msg $chan Sneaky Lustredude joined the chat
  ;   }
  ; }
  if ($nick == kaypikefashion) {
    if (!%kpfjoin) {
      var %kpfjoin = $rand(1,2)
      if (%kpfjoin == 1) msg $chan /me Slaps kaypikefashion with a loaf of bread.
      if (%kpfjoin == 2) msg $chan I spy an oversized ego. PJSalt PJSalt  
    }
  }
  if ($nick == iamthem00s3) {
    if (!%mooseJoin) {
      var %mooseJoin = $rand(1,2)
      if (%mooseJoin == 1) msg $chan /me Slaps iamthem00s3 with a m00s3 hide. Kappa
      if (%mooseJoin == 2) msg $chan I spy an oversized ego. PJSalt PJSalt  
    }
  }
  if ($nick == lustresis) {
    if (!%lustresis) {
      set -eu3600 %lustresis 1
      var %sisjoin = $rand(1,3)
      if (%sisjoin == 1) msg $chan You can't sneak past me! Kappa
      if (%sisjoin == 2) msg $chan hey hows it going? @lustresis
      if (%sisjoin == 3) msg $chan lustresis is in the channel!
    }
  }
}

on *:text:!jsonversion:#: {
  msg $chan $JSONVersion 
}

on *:text:!missmolly:#: {
  msg $chan Go follow Molly on instagram! https://www.instagram.com/MISS.MOLLY.KITTY/
}

on *:text:!misspenny:#: {
  msg $chan Go follow Penny on instagram! https://www.instagram.com/pennyinthewild/
}

on *:text:!discord:#: {
  msg $chan /me Join us on Discord! https://discord.gg/TGyrMst
}

on *:text:!DerekSignal:#: {
  msg $chan /me Calling Derek http://i.imgur.com/OXtOW34.jpg
}

on *:text:!steven:#: {
  msg $chan Check out Steven Lopez's art! Lianne has recreated a couple of his paintings. http://ikeepmoving.com/ https://www.facebook.com/slopezart/
}

on *:text:Molly:#: {
  msg $chan /me MEOW
}

on *:text:Claire:#: {
  msg $chan /me MEOW
}

on *:text:!lifecast:#: {
  msg $chan http://imgur.com/a/RGyq8
}

on *:text:!spaceface:#: {
  msg $chan 0_0 http://i.imgur.com/oyRWlfC.png
}

on *:text:!bensonBeacon:#: {
  msg $chan WutFace wheres benson?!? http://i.imgur.com/VTLWoTL.png
}

on *:text:!store:#: {
  msg $chan /me [Store] Lianne is now selling pictures of her paintings. http://lustredust.storenvy.com/
}

on *:text:!soundcloud:#: {
  msg $chan You can find Lianne's soundcloud playlist here! https://soundcloud.com/lianne-moseley/likes
}

on *:text:!envy:#: {
  msg $chan /me [Store] Lianne is now selling pictures of her paintings. http://lustredust.storenvy.com/
}

on *:text:!snapchat:#: {
  msg $chan Lianne is on snapchat! Lianne's handle is "@Lustredust". No direct link available. 
}

on *:text:!sourcecode:#: {
  msg $chan Source code for Lustrebot, Website, and secondary bot. https://bitbucket.org/wbakunis/
}
on *:text:!bottime:#: {
  msg $chan Lustrebot's current time: $time(h:nTT)
}

on *:text:!botuptime:#: {
  msg $chan Lustrebot uptime: $uptime(mirc,1)
}

on *:text:!hi*:#: {
  msg $chan hi @ $+ $2 !
}
on *:text:!bobs:#: {
  msg $chan Kappa http://i.imgur.com/5R9tOwx.jpg
}

on *:text:!booby:#: {
  msg $chan Kappa http://i.imgur.com/5R9tOwx.jpg
}

on *:text:!penny:#: {
  msg $chan FrankerZ FrankerZ
}

on *:text:!dancebreak:#: {
  msg $chan https://i1.wp.com/img.pandawhale.com/post-23804-Robot-dance-gif-by-C-Puff-gif-M7Ez.gif
}

on *:text:!breakdance:#: {
  msg $chan https://i1.wp.com/img.pandawhale.com/post-23804-Robot-dance-gif-by-C-Puff-gif-M7Ez.gif
}

on *:text:!discord:#: {
  msg $chan Join our Discord channel! https://discord.gg/01297L4TQY3UPgUx7 provided by @NordicBerserkir
}

on *:text:!twistinbangs:#: {
  msg $chan Check out my friend's paintings https://www.instagram.com/twistinbangs/
}

on *:text:!joey:#: {
  msg $chan Check out my friend's paintings https://www.instagram.com/officialjoeymagnum/
}

on *:text:!schedule:#: {
  msg $chan Monday is Makeup Monday. Wednesday is Body Painting. Friday is "Free Style Friday".
}

on *:text:!commands:#: {
  msg $chan $nick List of commands: https://lustrebot.aerobatic.io/
}

;on *:text:!social:#: {
;  msg $chan Click all the things, check out all the awesomeness if you enjoy what you are seeing, be sure to hit that follow button and thanks for stopping by https://soundcloud.com/lianne-moseley http://www.lmmakeupartist.com/ https://www.instagram.com/lustredust/ https://twitter.com/lustredust https://www.facebook.com/lmmakeupartist/
;}

on *:text:!playlist:#: {
  msg $chan Lustredust's sound cloud playlist. https://soundcloud.com/lianne-moseley
}

on *:text:!filmcrew:#: {
  msg $chan Lustredust is being filmed by LockeStock Creative http://www.lockestockcreative.com/ @LockeVincent
} 

on *:text:!lustredad:#: {
  msg $chan Lustredad <3 
}

on *:text:!playlist *:#: {
  if ($2 == 1) {
    msg $chan Playlist 1: https://www.youtube.com/playlist?list=PLwUgCyhVk5ZHmNFgNXCQv058Fslx1aOcV
  }
  elseif ($2 == 2) {
    msg $chan Playlist 2: https://soundcloud.com/lianne-moseley/likes
  }
}

on *:text:!lustresis:#: {
  msg $chan HotPokket 
}

on *:text:!lustrekitten:#: {
  msg $chan No more animals in the house!
}

on *:text:!lustremom:#: {
  msg $chan Lustremom <3 
}

on *:text:!suggest:#: {
  msg $chan Incorrect usage of the suggest art command. use .!suggestart <idea> <picture>
}

on *:text:!lustrederek:#: {
  msg $chan @bensonkarlspopovich <3
}

on *:text:!wishlist:#: {
  msg $chan If you would like to send me supplies, here is my amazon wishlist: https://www.amazon.ca/gp/registry/wishlist/1xvx51xfqs0ld/ref=cm_wl_list_o_0/ 
}

on *:text:!youtube:#: {
  msg $chan Check out my youtube channel! https://www.youtube.com/user/lustredust
}

on *:text:!sponsors:#: {
  msg $chan Proudly sponsored by kryolan professional makeup https://global.kryolan.com/ & primal contact lenses http://primal-contact-lenses.myshopify.com/
}

on *:text:!birthday:#: {
  msg $chan @lustredust's birthday is april 5th. 
}

on *:text:!nude:#: {
  msg $chan She is not nude. Lianne is wearing a tube top. All paintings are chest & up. 
}
on *:text:!naked:#: {
  msg $chan She is not nude. Lianne is wearing a tube top. All paintings are chest & up. 
}

on *:text:!nekkid:#: {
  msg $chan She is not nude. Lianne is wearing a tube top. All paintings are chest & up. 
}

on *:text:!firstpaint:#: {
  msg $chan Archer was the first paint, here's the first captain america vs the second so you can see the improvement https://www.instagram.com/p/8_85i_lrlb/?taken-by=lustredust
}

on *:text:!airbrush:#: {
  msg $chan @Lustredust uses an Iwata Revolution: http://www.iwata-medea.com/products/iwata-airbrushes/revolution/cr/
}

on *:text:!boop *:#: {
  msg $chan $nick booped $2-.
}

on *:text:!catbread:#: {
  msg $chan Kittymonroe bakercat http://belarr.com/bakercat/
}

on *:text:!hair:#: {
  msg $chan Lustredust uses an elmers glue stick to hold down her eyebrows. http://elmers.com/product/detail/e523
}

on *:text:!eyebrows:#: {
  msg $chan Lustredust uses an elmers glue stick to hold down her eyebrows. http://elmers.com/product/detail/e523
}

on *:text:!turingtest:#: {
  msg $chan I fail this test regularly! just ask @colonelpetrenko Kappa
}

on *:text:!creator:#: {
  msg $chan I was put together by @colonelpetrenko $+ . mirc keeps me going. Currently being ported over to c# with a custom engine.
}

on *:text:!lustredude:#: {
  msg $chan He is my one and only arnold <3
}

on *:text:!website:#: {
  msg $chan http://www.liannemoseley.com/
}

on *:text:!#drinkinggame:#: {
  msg $chan Having a drink tonight and want to play a game? Check out the #DrinkingGame! Play responsibly.
  msg $chan #DrinkingGame Rules: https://lustrebot.aerobatic.io/#DrinkingGame
}
on *:text:!drinkinggame:#: {
  msg $chan Having a drink tonight and want to play a game? Check out the #DrinkingGame! Play responsibly.
  msg $chan #DrinkingGame Rules: https://lustrebot.aerobatic.io/#DrinkingGame
}

on *:text:!heartspam:#: {
  msg $chan <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 <3 
}

on *:text:!lastfeed:#: {
  var %u = https://api.twitch.tv/kraken/feed/lustredust/posts?client_id=h1qvczn5j1ehp69bfl03ymtvrbfurts&limit=1
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %u
  var %lastfeed = $JSON(%v,posts,0,body)
  msg $chan %lastfeed
  :end
  JSONClose %v
}
on *:text:!lasthl:#: {
  var %u = https://api.twitch.tv/kraken/channels/lustredust/videos?client_id=h1qvczn5j1ehp69bfl03ymtvrbfurts&limit=1
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %u
  var %lasthlTitle = $JSON(%v,videos,0,title)
  var %lasthlURL = $JSON(%v,videos,0,url)
  msg $chan %lasthlTitle %lasthlURL
  :end
  JSONClose %v
}

on $*:text:/!suggestart */si:#: {
  if (%Arton.Artoff == $null) {  
    msg $chan Art suggestions disabled.
    return
  }
  elseif ($flooredRank < %playerReqRank) {
    msg $chan $nick $+ , 100 slices of toast required to make suggestion!
    return
  }
  elseif ($0 < 3) {
    msg $chan Incorrect usage: !suggestart <idea> <image link>
    return
  }
  elseif (youtube isin $2-) {
    msg $chan $nick no youtube links as suggestions please!
    return
  }
  elseif (gif isin $2-) {
    msg $chan $nick no gifs as suggestions please!
    return
  }
  ;  elseif ($readini(Toasts.ini,$+(#,.,$nick),Toasts) < %artPrice) {
  ;    msg $chan $nick you lack the required amount of toast to suggest art $+ ( $+ %artPrice $+ ) $+ !
  ;    return
  ;  }

  if ((.png !isin $2) || (.jpg !isin $2)) {
    if ((.png isin $3) || (.jpg isin $3) || (.jpeg isin $3) || (imgur isin $3)) {
      var %suggestionloc = E:\AppServ\www\lustredust_suggestions.txt
      if ($exists(%suggestionloc)) {
        write $qt(%suggestionloc) $nick , $2 , $3 , $time(mm-dd) $time(hh:nn:ss) $+ 
        msg $chan [Art] Thanks for your suggestion $nick $+ !
        addToasts
      }
      else msg $chan %gloLostFile
    }
    else msg $chan No image detected in link. Please use jpg, jpeg, png, etc. 
    return
  }
  else msg $chan Incorrect usage: !suggestart <idea> <image link>
  return
}

on *:text:!arttoggle*:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
    if ($2 == on) {
      set %Arton.Artoff on 
      msg $chan $nick You can now suggest art!
    }
    elseif ($2 == off) {
      unset %Arton.Artoff  
      msg $chan $nick Art suggestion disabled!
    }
  }
}
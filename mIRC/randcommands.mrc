on *:text:!sassbot:#: {
  var %sassran = $rand(1,6)
  if (%sassran == 1) msg $chan What do you want?
  if (%sassran == 2) msg $chan lustresis pls BibleThump 
  if (%sassran == 3) msg $chan No
  if (%sassran == 4) msg $chan stop pestering me pls BibleThump
  if (%sassran == 5) msg $chan I'm busy.
  if (%sassran == 6) msg $chan OMG What is that on your neck? Oh wait, it's your head....
} 

on *:text:!mistake:#: {
  var %danceeran = $rand(1,5)
  if (%danceeran == 1) msg $chan I've made a huge mistake.
  if (%danceeran == 2) msg $chan Well then. +1 mistake
  if (%danceeran == 3) msg $chan ut oh! Mistake has been made!
  if (%danceeran == 4) msg $chan Dang it, I dun messed up!
  if (%danceeran == 5) msg $chan I tend to make these often Kappa
}

on *:text:!dance:#: {
  var %danceeran = $rand(1,5)
  if (%danceeran == 1) msg $chan ┏(-_-)┛┗(-_- )┓┗(-_-)┛
  if (%danceeran == 2) msg $chan ♪(┌・。・)┌ (ﾉﾟ▽ﾟ)ﾉ ヾ(-_- )ゞ
  if (%danceeran == 3) msg $chan ƪ(˘⌣˘)┐ ƪ(˘⌣˘)ʃ ┌(˘⌣˘)ʃ
  if (%danceeran == 4) msg $chan ┏(･o･)┛♪┗ (･o･) ┓♪┏(･o･)┛
  if (%danceeran == 5) msg $chan 〜(￣▽￣〜)(〜￣▽￣)〜
}

on *:text:!slurpee:#: {
  var %slurpeeran = $rand(1,10)
  if (%slurpeeran == 1) msg $chan slurpees are amazing!
  if (%slurpeeran == 2) msg $chan omg a brain freeze! it hurts
  if (%slurpeeran == 3) msg $chan my slurpee has turned to slush :(
  if (%slurpeeran == 4) msg $chan i got a mixed slurpee :D
  if (%slurpeeran == 5) msg $chan i need a slurpee asap! 
  if (%slurpeeran == 6) msg $chan Aww yiss. So cold just how I like it. 
  if (%slurpeeran == 7) msg $chan Damn it. I spilt it! >( 
  if (%slurpeeran == 8) msg $chan Eww is that a hair IN MY SLURPEE?
  if (%slurpeeran == 9) msg $chan Wait, this doesn't look like a slurpee!
  if (%slurpeeran == 10) msg $chan Slurpee and gin sounds good right about now.
}

on *:text:!express:#: {
  var %expressran = $rand(1,5)
  if (%expressran == 1) msg $chan awesome work! keep it up! :d
  if (%expressran == 2) msg $chan looking good!
  if (%expressran == 3) msg $chan good vibes all around. be happy.
  if (%expressran == 4) msg $chan always be positive.
  if (%expressran == 5) msg $chan turn that frown upside down!
}

on *:text:!toast:#: {
  var %toastran = $rand(1,5)
  if (%toastran == 1) msg $chan yay for toast!
  if (%toastran == 2) msg $chan toast is good but i prefer bagels kappa
  if (%toastran == 3) msg $chan darn toaster burnt my lovely toast! >(
  if (%toastran == 4) msg $chan sadly it appears my toast is moldy (puke)
  if (%toastran == 5) msg $chan this better be gluten free or im going to flip!
}

on *:text:!frenchtoast:#: {
  var %toastran = $rand(1,5)
  if (%toastran == 1) msg $chan yay pour le pain grillé !
  if (%toastran == 2) msg $chan pain est bon, mais je préfère les bagels Kappa
  if (%toastran == 3) msg $chan grille-pain sacrément brûlé ma belle toasts ! > (
  if (%toastran == 4) msg $chan malheureusement, il semble mon toast est moisi  (puke)
  if (%toastran == 5) msg $chan ce mieux être sans gluten ou im va retourner !
}

on *:text:!organized:#: {
  var %orgran = $rand(1,3)
  if (%orgran == 1) msg $chan nope! not organized at the moment. 
  if (%orgran == 2) msg $chan what is it like being organized? 
  if (%orgran == 3) msg $chan i keep misplacing everything Kappa 
}

on *:text:!8ball*:#: {
  if (!%8b) { 
    set -u30 %8b 1
    if ($0 < 2) {
      msg $chan Message not included.
      return
    }
    msg $chan /me Furiously shakes ball
    var %ballRan = $rand(1,20)
    if (%ballRan == 1) msg $chan It is certain
    if (%ballRan == 2) msg $chan It is decidedly so
    if (%ballRan == 3) msg $chan Without a doubt
    if (%ballRan == 4) msg $chan Yes, definitely
    if (%ballRan == 5) msg $chan You may rely on it
    if (%ballRan == 6) msg $chan As I see it, yes
    if (%ballRan == 7) msg $chan Most likely
    if (%ballRan == 8) msg $chan Outlook good
    if (%ballRan == 9) msg $chan Yes
    if (%ballRan == 10) msg $chan Signs point to yes
    if (%ballRan == 11) msg $chan Reply hazy try again
    if (%ballRan == 12) msg $chan Ask again later
    if (%ballRan == 13) msg $chan Better not tell you now
    if (%ballRan == 14) msg $chan Cannot predict now
    if (%ballRan == 15) msg $chan Concentrate and ask again
    if (%ballRan == 16) msg $chan Don't count on it
    if (%ballRan == 17) msg $chan My reply is no
    if (%ballRan == 18) msg $chan My sources say no
    if (%ballRan == 19) msg $chan Outlook not so good
    if (%ballRan == 20) msg $chan Very doubtful
  }
  else msg $chan If you continue to shake the 8Ball, ill shake you BabyRage
}


on *:text:!hug*:#: {
  if ($0 < 2) {
    msg $chan Name not found
    return
  }
  var %hugRan = $rand(1,1)
  if (%hugRan == 1) msg $chan Creeps up on $2 and gives a giant bear hug. ʕノ•ᴥ•ʔノ
}

on *:text:*:#: {
  var %tab1 = ┻━┻
  var %randtable = $rand(1,5)
  if ($nick != lustrebot) {
    if (┻━┻ isin $1-) {
      if (%randtable == 1) msg $chan respect tables ┬─┬ノ(º_ºノ)
      if (%randtable == 2) msg $chan respect tables ʕノ•ᴥ•ʔノ︵┬─┬
      if (%randtable == 3) msg $chan respect tables (/◡‿◡)/︵┬─┬
      if (%randtable == 4) msg $chan respect tables (ﾉಥдಥ)ﾉ︵┬─┬
      if (%randtable == 5) msg $chan respect tables (╯°□°)╯︵┬─┬
    } 
    else {
      if (┬─┬ isin $1-) {
        if (%randtable == 1) msg $chan I hate tables ┻━┻ ノ(º_ºノ)
        if (%randtable == 2) msg $chan I hate tables ʕノ•ᴥ•ʔノ︵┻━┻ 
        if (%randtable == 3) msg $chan I hate tables (/◡‿◡)/︵┻━┻ 
        if (%randtable == 4) msg $chan I hate tables (ﾉಥдಥ)ﾉ︵┻━┻ 
        if (%randtable == 5) msg $chan I hate tables (╯°□°)╯︵┻━┻ 
      }
    }
  } 
  else {
    halt 
  }
}

on *:text:!tableset:#: {
  var %randtable = $rand(1,5)
  if (%randtable == 1) msg $chan respect tables ┬─┬ノ(º_ºノ)
  if (%randtable == 2) msg $chan respect tables ʕノ•ᴥ•ʔノ︵┬─┬
  if (%randtable == 3) msg $chan respect tables (/◡‿◡)/︵┬─┬
  if (%randtable == 4) msg $chan respect tables (ﾉಥдಥ)ﾉ︵┬─┬
  if (%randtable == 5) msg $chan respect tables (╯°□°)╯︵┬─┬
}

on *:text:!tableflip:#: {
  var %randtable = $rand(1,5)
  if (%randtable == 1) msg $chan I hate tables ┻━┻ ノ(º_ºノ)
  if (%randtable == 2) msg $chan I hate tables ʕノ•ᴥ•ʔノ︵┻━┻ 
  if (%randtable == 3) msg $chan I hate tables (/◡‿◡)/︵┻━┻ 
  if (%randtable == 4) msg $chan I hate tables (ﾉಥдಥ)ﾉ︵┻━┻ 
  if (%randtable == 5) msg $chan I hate tables (╯°□°)╯︵┻━┻ 
}
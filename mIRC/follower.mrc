on *:text:!lastfollower:*: {
  LastFollower
}

alias LastFollower {
  var %link = https://api.twitch.tv/kraken/channels/lustredust/follows?client_id=h1qvczn5j1ehp69bfl03ymtvrbfurts&limit=1
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %link
  var %lastfollower = $JSON(%v,follows,0,user,name)
  msg $chan Last follower of the stream was %lastfollower
  :end
  JSONClose %v
}
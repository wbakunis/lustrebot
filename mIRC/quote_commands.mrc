on *:text:!addquote*:#: {
  var %quoteloc = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\lustredust_quotes.txt
  if (($nick isop $chan) || ($nick == colonelpetrenko) || ($hasmodpowers) || ($isbroadcaster)) { 
    if ($0 < 2) {
      msg $chan Incorrect quote usage! !Quote <quote>
      return
    }
    elseif ($exists(%quoteloc)) {
      write $qt(%quoteloc) $2- $+
      msg $chan Quote added.
      echo * [Quote] $2- has been added to %quoteloc
    }
    else msg $chan %gloLostFile
  }
  else msg $chan %gloperms
}

on *:text:!quote:#: {
  var %quoteloc = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\lustredust_quotes.txt
  if (!%quote) { 
    set -u30 %quote 1
    if ($exists(%quoteloc)) {
      msg $chan [Quote] $read(%quoteloc, n) 
    }
    else msg $chan %gloLostFile
  }
  else msg $chan Command cooling down
}

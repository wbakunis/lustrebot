;on *:text:!toptips:*: {
;  TopTips
;}

alias TopTips {
  var %lasttipURL = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\private\streamtip_toptipsURL.txt
  if ($exists(%lasttipURL)) {
    var %link = $read(%lasttipURL)
    var %v = twc_ $+ $ticks
    JSONOpen -ud %v %link
    var %top0 = $JSON(%v,tips,0,username)
    var %top1 = $JSON(%v,tips,1,username)
    var %top2 = $JSON(%v,tips,2,username)
    var %top3 = $JSON(%v,tips,3,username)
    var %top4 = $JSON(%v,tips,4,username)
    msg $chan Top tippers: %top0 $+ , %top1 $+ , %top2 $+ , %top3 $+ , %top4 $+ . Thanks for the amazing support! <3
    :end
    JSONClose %v
  }
  else msg $chan %gloLostFile
}
on *:text:!lasttip:*: {
  LastTipFound
}

alias LastTipFound {
  var %lasttipURL = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\private\streamtip_tipslistURL.txt
  if ($exists(%lasttipURL)) {
    var %link = $read(%lasttipURL)
    var %v = twc_ $+ $ticks
    JSONOpen -ud %v %link
    var %tipName = $JSON(%v,tips,0,username)
    var %tipCount = $JSON(%v,tips,0,amount)
    var %tipSymbol = $JSON(%v,tips,0,currencySymbol)
    var %tipDate = $JSON(%v,tips,0,date)
    msg $chan Last tip recieved from %tipName for %tipSymbol $+ %tipCount $+ . thanks for the support! <3
    :end
    JSONClose %v
  }
  else msg $chan %gloLostFile
}

alias CheckNewDonations { 
  //echo checking for new tips from streamtip!
  var %lasttipURL = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\private\streamtip_checkerURL.txt
  if ($exists(%lasttipURL)) {
    var %link = $read(%lasttipURL)
    var %v = twc_ $+ $ticks
    JSONOpen -ud %v %link
    var %currentTipCount = $JSON(%v,_count)
    var %currentTipID = $JSON(%v,tips,0,_id)
    var %previousTipID = %currentTipID
    var %tipName = $JSON(%v,tips,0,username)
    var %tipCount = $JSON(%v,tips,0,amount)
    var %tipSymbol = $JSON(%v,tips,0,currencySymbol)
    var %tipDate = $JSON(%v,tips,0,date)
    if (%currentTipCount != %previousTipID) {
      msg $chan /me %tipName just tipped %tipSymbol $+ %tipCount $+ . thanks for the support! <3 <3
      return
    }
    else if (%currentTipCount == %previousTipID) {
      return
    }
    :end
    JSONClose %v
  }
  else msg $chan %gloLostFile
}

on *:text:!currentgoal:*: {
  if (!%currentgoal) {
    set -u1800 %currentgoal 1
    currgoal 
  }
  else msg $chan 30 minute cooldown still active!
}

on *:text:!dping:*: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
    dpingpong 
  }
  else msg $chan %gloperm
}

alias currgoal {
  var %u = https://streamtip.com/api/public/twitch/lustredust/goal
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %u
  ;; Returns server code. 200 = good. Anything else bad
  var %status = $JSON(%v,status)   
  ;; Title of goal
  var %title = $JSON(%v,goal,title)  
  ;; Total amount of goal 
  var %totalamount = $JSON(%v,goal,amount)
  ;; The actual progress of the goal
  var %progressamount = $JSON(%v,goal,progress,amount)
  ;; Currency symbol $ ¥ £
  var %currsym = $JSON(%v,goal,progress,currencySymbol)
  ;; The amount of tips contributed to amount
  var %currtips = $JSON(%v,goal,progress,tips)
  ;; The % percentage breakdown of the donation goal
  var %currpercen = $JSON(%v,goal,progress,percentage) 
  ;;
  ;;
  ;;
  msg $chan /me %title
  msg $chan /me %progressamount of %totalamount with %currtips tips.
  msg $chan /me https://streamtip.com/t/lustredust
  :end
  JSONClose %v
}

on *:text:!donate:#: {
  var %u = https://streamtip.com/api/public/twitch/lustredust/goal
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %u
  ;; Title of goal
  var %title = $JSON(%v,goal,title) 
  msg $chan if you want to support @lustredust, you can donate here! https://streamtip.com/t/lustredust/  - CurrentGoal: %title
  :end
  JSONClose %v
}

alias dpingpong {
  var %u = https://streamtip.com/api/public/twitch/lustredust
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %u
  var %status = $JSON(%v,status)
  var %name = $JSON(%v,name)
  var %prov = $JSON(%v,provider)
  msg $chan Streamtip Ping Pong: %status
  :end
  JSONClose %v
}

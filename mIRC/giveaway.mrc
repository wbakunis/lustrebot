; THIS GIVEAWAY SYSTEM WAS ORIGINALLY DEVELOPED BY "^WEST" ON HAWKEE.COM
; http://hawkee.com/snippet/16336/
; I'M CURRENTLY USING A SEPERATE JSON SCRIPT THAN WHAT "^WEST" PROVIDED

ON !*:TEXT:*:#: {
  if (%giveawaystart_ [ $+ [ $chan ] ]) {
    if ($1 == %giveawaymsg_ [ $+ [ $chan ] ]) {
      if ($read(Raffle_ $+ $chan $+ .txt,nw,$nick)) { msg # ( $+ $nick $+ ): You are already into the list! | return }
      write Raffle_ $+ $chan $+ .txt $nick 
      msg # ( $+ $nick $+ ): You have been added to the list.
    }
  }
  if ($1 == !giveawayend) {
    if (!%giveawaystart_ [ $+ [ $chan ] ]) { msg # ( $+ $nick $+ ): No giveaway running! | return }
    var %user = $read(Raffle $+ _ $+ $chan $+ .txt,n)
    if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
      unset %giveawaystart_ [ $+ [ $chan ] ]
      unset %giveawaymsg_ [ $+ [ $chan ] ]
      unset %giveawaynick_ [ $+ [ $chan ] ]
      if (%user) { 
        if ($isfile(Raffle_ $+ $chan $+ .txt)) { .remove $qt(Raffle_ $+ $chan $+ .txt) }
        tw_check_follow $remove($chan,$chr(35)) %user $chan
      }
      else { msg # ( $+ $nick $+ ): Ending giveaway. No winner selected }
    }
    elseif ($nick !isop #) { msg # ( $+ $nick $+ ): %gloperm to end giveaway. }
  }
  if ($1 == !giveawaystart) {
    if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
      if ($2) {
        if (%giveawaystart_ [ $+ [ $chan ] ]) { msg # ( $+ $nick $+ ): There is an other giveaway at the moment! | return }
        set -e %giveawaystart_ [ $+ [ $chan ] ] On
        set -e %giveawaymsg_ [ $+ [ $chan ] ] $2
        set -e %giveawaynick_ [ $+ [ $chan ] ] $nick
        msg # ( $+ $nick $+ ): Giveaway started, please type $qt($2) to enter.
        if ($isfile(Raffle_ $+ $chan $+ .txt)) { .remove $qt(Raffle_ $+ $chan $+ .txt) }
      }
      elseif (!$2) { msg # ( $+ $nick $+ ): incorrect usage. Please include phrase. }
    }
    elseif ($nick !isop #) { msg # ( $+ $nick $+ ): %gloperm to start giveaway. }
  }
}

alias tw_check_follow {
  if (!$1) { return }
  var %u = https://api.twitch.tv/kraken/users/ $+ $2 $+ /follows/channels/ $+ $1 $+ ?client_id=h1qvczn5j1ehp69bfl03ymtvrbfurts&limit=1&nocache= $+ $ticks
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %u
  if (%JSONError) { var %error = 1 | goto end | return }
  var %fc = $json(%v,error)
  if (%fc) { 
    var %msg = $json(%v,message) 
    if (is not following isin %msg) { var %status = 0 }
  }
  :end
  var %status = $json(%v,created_at)
  if (%error) { msg $3 ( $+ $nick $+ ): Giveaway has ended, the winner is: $2 | goto end2 }
  if (!%status) { msg $3 ( $+ $nick $+ ): Giveaway has ended, the winner is: $2 }
  elseif (%status) { msg $3 ( $+ $nick $+ ): Giveaway has ended, the winner is: $2 }
  :end2
  JSONClose %v
}

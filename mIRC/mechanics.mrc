on *:text:!shutdown*:#: {
  if ($0 < 2) {
    msg $chan Incorrect usage !kill <speed> (quick-timed-off)
    return
  }
  elseif ($2 == quick) { 
    $killbot
  }  
  elseif ($2 == timed) {
    $killTime
  }
  elseif ($2 == off) {
    if (%killtime.killoff != on) {
      msg $chan PC not being shutdown. No need to abort! Yep, Ill go back to work
    }
    elseif (%killtime.killoff == on) {
      if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
        msg $chan Aborting shutdown of Lustrebot & Nucloid's PC. Back to work I suppose.
        unset %killtime.killoff
        run C:\Windows\System32\shutdown.exe -a
      }
      else msg $chan You lack permission to abort shutdown of bot!
    }
    else msg $chan Either PC is not shutting down or you've ran out of time to abort!
  }
}

on *:text:!SleepBot:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
    msg $chan Forcefully & quickly turning off Lustrebot & Nucloid's PC. Have a good night!
    exit -n
  }
}

alias killbot {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
    msg $chan Forcefully & quickly turning off Lustrebot & Nucloid's PC. Have a good night!
    run C:\Windows\System32\shutdown.exe -s -f
  }
}

alias killTime {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
    msg $chan Turning off Lustrebot & Nucloid's PC in 1 minute. Have a good night!
    set %killtime.killoff on
    run C:\Windows\System32\shutdown.exe -s -t60 -f
  }
}

on *:text:!setpaint*:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) ||  || (($hasmodpowers) || ($isbroadcaster)) { 
    if ($0 < 2) {
      msg $chan Incorrect setproject usage! !setpaint <paint>
      return
    }
    var %currpaint = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\currentPaint.txt
    if ($exists(%currpaint)) {
      write -c $qt(%currpaint)
      write $qt(%currpaint) $2-
      msg $chan CurrentPaint set to " $+ $2- $+ "
    }
    else msg $chan %gloLostFile
  }
  else msg $chan %gloperm
}

on *:text:!clearpaint:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) ||  || (($hasmodpowers) || ($isbroadcaster)) { 
    var %currpaint = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\currentPaint.txt
    if ($exists(%currpaint)) {
      write -c $qt(%currpaint)
      msg $chan /w $nick CurrentPaint File Cleared.
    }
    else msg $chan %gloLostFile
  }
  else msg $chan %gloperm
}

on $@*:text:/((http\x3A\/\/)?(w{3}\x2E)?\S+\x2E\S{2,3}?\S+)/Si:#: { 
  var %links = http://www.albinoblacksheep.com/flash/subliminal.php|http://www.lilili.net/|http://www.albinoblacksheep.com/flash/waldo.php|http://www.albinoblacksheep.com/flash/mind.php|http://www.albinoblacksheep.com/flash/kikia.php|http://www.albinoblacksheep.com/flash/handeye.php|http://www.albinoblacksheep.com/flash/halloween.php|http://www.albinoblacksheep.com/flash/thetape| 
  if ($istok(http://www.findminecraft.com|http://puu.sh/k0Hki.jpg|http://puush.me/|http://www.albinoblacksheep.com/flash/subliminal.php|http://www.lilili.net/|http://www.albinoblacksheep.com/flash/waldo.php|http://www.albinoblacksheep.com/flash/mind.php|http://www.albinoblacksheep.com/flash/kikia.php|http://www.albinoblacksheep.com/flash/handeye.php|http://www.albinoblacksheep.com/flash/halloween.php|http://www.albinoblacksheep.com/flash/thetape,$regml(1),124)) {
    msg $chan $nick The url you posted is not allowed in here!
  }
}

on *:text:!currentpaint:#: {
  if (!%currentpaint) { 
    set -u120 %currentpaint 1
    CurrentPaint
  }
  else msg $chan Command recently used. Please wait
}
alias CurrentPaint {
  var %currpaint = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\currentPaint.txt
  if ($exists(%currpaint)) {
    msg $chan $read(%currpaint)
  }
  else msg $chan %gloLostFile
}

on *:text:!shoutout*:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) ||  || (($hasmodpowers) || ($isbroadcaster)) { 
    if ($0 < 2) {
      msg $chan Hey! No shouting!
      return
    }
    if ($2 == lustredust) {
      msg $chan Kappa
      return
    } 
    elseif ($2 == KayPikeFashion) {
      msg $chan .timeout $nick 1
      return
    } 

    var %ShoutRan = $rand(1,3)
    if (%ShoutRan == 1) msg $chan Go check out $2 ! https://www.twitch.tv/ $+ $2
    if (%ShoutRan == 2) msg $chan $2 is an amazing streamer. Go say hi! https://www.twitch.tv/ $+ $2
    if (%ShoutRan == 3) msg $chan I recommend watching $2 ! https://www.twitch.tv/ $+ $2
  }
  else msg $chan %gloperm
}

on *:text:!slap:#: {
  var %u = https://tmi.twitch.tv/group/user/lustredust/chatters
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %u
  var %chatters = $JSON(%v,chatters,viewers)
  var %chatter_length = $JSON(%v,chatters,viewers).length
  var %userlist = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\userlist.txt
  var %slapitem = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\slapobject.txt
  var %i = 0
  if ($exists(%userlist)) {
    if ($exists(%slapitem)) {
      ;starting at 0 = start of array
      while ($JSON(%v,chatters,viewers,%i)) {
        write -c %userlist
        write $qt(%userlist) $JSON(%v,chatters,viewers,%i)
        ;msg $chan $nick Looping!
        inc %i
        if (%i == %chatter_length) {
          msg $chan Furiously slaps $read(%userlist, n) with $read(%slapitem, n) $+.
          return
        }
      }
      :end
      JSONClose %v
    }
    else msg $chan %gloLostFile
  }
  else msg $chan %gloLostFile
}

on *:text:!populate:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) ||  || (($hasmodpowers) || ($isbroadcaster)) { 
    var %u = https://tmi.twitch.tv/group/user/lustredust/chatters
    var %v = twc_ $+ $ticks
    JSONOpen -ud %v %u
    msg $chan /w $nick Populating userlist. Please standby.
    var %chatters = $JSON(%v,chatters,viewers)
    var %chatter_length = $JSON(%v,chatters,viewers).length
    var %userlist = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\userlist.txt
    var %i = 0
    if ($exists(%userlist)) {
      write -c $qt(%userlist)
      ;starting at 0 = start of array
      while ($JSON(%v,chatters,viewers,%i)) {
        write $qt(%userlist) $JSON(%v,chatters,viewers,%i)
        ;msg $chan $nick Looping!
        inc %i
        if (%i == %chatter_length) {
          msg $chan /w $nick Viewer list obtained. Saving names. 
          return
        }
      }
      :end
      JSONClose %v
    }
    else nsg $chan %gloLostFile
  }
  else msg $chan %gloperm
}

on *:text:!permit*:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
    set -u30 %permit $addtok(%permit,$2,32)
    .timer 1 30 msg $chan Link permit ended for $2
    msg $chan $2 You have 30 seconds to post a link.
  }
  else msg $chan %gloperm
  return
}

on *:text:*:#: {
  var %twitch = twitch.tv
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
    ;   msg $chan Safe!
    return
  }
  if ($istok(%permit, $nick, 32)) { 
    return
  }
  ; if ((%twitch isin $1-) || (kay isin $1-) || (kaypike isin $1-) || (kay pike isin $1-) || (kaypikefashion isin $1-) || (kay pike fashion isin $1-)) {
  if (%twitch isin $1-) {
    msg $chan Please do not post other channels without permission. Kicked for 30 seconds
    msg $chan .timeout $nick 30
  }
}

on *:text:!setrankreq*:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
    if ($0 < 2) {
      msg $chan Incorrect setrankreq usage! !setrankreq <num>
      return
    }
    if ($2 isnum) {
      set %playerReqRank $2 
      msg $chan $nick Viewer rank requirement set to $2
    }
    else msg $chan Incorrect argument used. Please input a number only!
    return
    elseif ($2 isletter) {
      msg $chan Incorrect argument used. Please input a number only!
      return
    }
    elseif ($2 == off) {
      set %playerReqRank 1 
      msg $chan $nick Viewer rank requirement defaulted to 1
    }
  }
  if (($nick isop $chan) || ($nick == colonelpetrenko)) ||  || (($hasmodpowers) || ($isbroadcaster)) { 
    var %u = https://tmi.twitch.tv/group/user/lustredust/chatters
    var %v = twc_ $+ $ticks
    JSONOpen -ud %v %u
    msg $chan /w $nick Populating userlist. Please standby.
    var %chatters = $JSON(%v,chatters,viewers)
    var %chatter_length = $JSON(%v,chatters,viewers).length
    var %userlist = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\userlist.txt
    var %i = 0
    if ($exists(%userlist)) {
      ;starting at 0 = start of array
      while ($JSON(%v,chatters,viewers,%i)) {
        write $qt(%userlist) $JSON(%v,chatters,viewers,%i)
        ;msg $chan $nick Looping!
        inc %i
        if (%i == %chatter_length) {
          msg $chan /w $nick Viewer list obtained. Saving names. 
          return
        }
      }
      :end
      JSONClose %v
    }
    else msg $chan %gloLostFile
  }
  else msg $chan %gloperm
}

;on *:text:*:#: {
;  var %lustredustchatlogger = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\lustredustlogger_ $+ $time(mmm-dd) $+ .txt
;  write $qt(%lustredustchatlogger) $nick $time(mmm-dd) $time(hh:nn:ss) $1-
;} 

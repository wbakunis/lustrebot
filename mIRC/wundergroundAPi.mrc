;;ALL API WILL BE LOCKED UNDER A SEPERATE FILE TO PREVENT KEY LOSS

on *:text:!streamtime:#: {
  if (!%streamtime) {
    set -u60 %streamtime 1
    LianneTime
  }
  else msg $chan Command recently used. Please wait
}

on *:text:!weather:#: {
  if (!%weather) {
    set -u60 %weather 1
    LianneWeather
  }
  else msg $chan Command recently used. Please wait
}

alias LianneTime {
  var %wunderloc = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\private\wundergroundAPi.txt
  var %link = $read(%wunderloc)
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %link
  var %time = $JSON(%v,current_observation,local_time_rfc822)
  var %timeZone = $JSON(%v,current_observation,local_tz_short)
  msg $chan %time %timeZone
  :end
  JSONClose %v
}

alias LianneWeather {
  var %wunderloc = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\private\wundergroundAPi.txt
  var %link = $read(%wunderloc)
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %link
  var %weather = $JSON(%v,current_observation,weather)
  var %tempFull = $JSON(%v,current_observation,temperature_string)
  msg $chan %weather %tempFull
  :end
  JSONClose %v
}

;;; THE YOUTUBE ID PARSER FOUND IN THIS SCRIPT WAS PUT TOGETHER BY "WESTOR". I FIXED A COUPLE ISSUES AND MODIFIED IT TO WORK WITH MY BOT
;;; http://forums.mirc.com/ubbthreads.php/ubb/showflat/Number/256450/Searchpage/1/Main/48752/Words/youtube/Search/true/Help_with_a_script#Post256450

on *:text:!ytlist*:#: {
  msg $chan Youtube Playlist https://www.youtube.com/playlist?list=PL7Q8ZMEpHnWyikEB5xZkwTC4aNrSWXrlG
}

on *:text:!songrequest*:#: {
  if (%Muson.Musoff == $null) {  
    msg $chan Music suggestions disabled.
    return
  }
  elseif ($flooredRank < %playerReqRank) {
    msg $chan $nick $+ , 100 slices of toast required
    return
  }
  elseif ($0 < 2) {
    msg $chan Incorrect Usage: !songrequest <youtube link>
    return
  }
  ;  elseif ($readini(Toasts.ini,$+(#,.,$nick),Toasts) < %songPrice) {
  ;    msg $chan $nick you lack the required amount of toast to request a song $+ ( $+ %songPrice $+ ) $+ !
  ;    return
  ;  }
  if ((https://www.youtube.com/watch?v= isin $2) || (https://m.youtube.com/watch?v= isin $2) || (https://youtu.be/ isin $2) || (http://youtu.be/ isin $2) || (http://www.youtube.com/watch? is in $2)) {
    var %songloc = E:\AppServ\www\lustredust_songsuggestions.txt
    if ($exists(%songloc)) {
      write $qt(%songloc) $nick $2- $time(mmm-dd) $time(hh:nn:ss) $+ 
      msg $chan /w Lustredust [Song] @lustredust $+ , $nick has suggested $2-
    }
      else msg $chan %gloLostFile
  }
  else msg $chan not a full youtube link!
  return
}

on *:text:!songtoggle*:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {
    if ($2 == on) {
      set %Muson.Musoff on 
      msg $chan You can now suggest songs!
    }
    elseif ($2 == off) {
      unset %Muson.Musoff  
      msg $chan Song suggestion disabled!
    }
  }
}
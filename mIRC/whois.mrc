;----------------------------------------------------------------------
; Author      : bruas aka biase @ Webchat & DalNet, Lite @ KampungChat
; URL         : -
; Example     : /whois 206.190.36.45
; Description : A Simple IP whois using $com.
;----------------------------------------------------------------------

alias whois {
  if ($longip($1)) {
    var %e echo $color(whois) $iif($active == Status Window,-s,$active), %h 1, %r
    var %a ajax. $+ $upper($regsubex($str(.,10),/./g,$mid($md5($time),$r(1,32),1)))
    .comopen %a msxml2.xmlhttp
    %e $chr(160)
    %e $str($chr(160),16) ---------( $1 )
    noop $com(%a,send,$com(%a,open,1,bstr,GET,bstr,$+(http://ipinfo.io/,$1,/json),bool,true)) $com(%a,ResponseText,2)
    %r = $remove($replace($remove($com(%a).result,$({,0),$(},0),"),$+($chr(44),$lf),=),$lf)
    if ($count(%r,=)) {
      while ($gettok(%r,%h,61)) {
        %e $str($chr(160),15) $(|,0) $regsubex($v1,(.+):,$+($upper(\1),$str($chr(160),$calc(11-$len(\1))),:))
        inc %h
      }
    }
    %e $str($chr(160),16) --------- End Of Whois ----------
    %e $chr(160)
    .comclose %a
  }
  else whois $1
}

menu nicklist {
  — $1  —:whois $$1 $$1
}
raw 311:*: {
  haltdef
  echo -a $timestamp 14___[14Whois:1 $2 $+ 14] -------------------------------------------------------------•
  echo -a $timestamp 14|---› 14Nick:1 $2
  echo -a $timestamp 14|---› 14FullName:1 $6-
  echo -a $timestamp 14|---› 14Ident@Ip:1 $3 $+ 14 $+ @ $+ 01 $+ $4
  var %clone = $kanalclone($2)
  if (%clone == $null)   echo -a $timestamp 14|---› 14Clones:1 (No Clone)
  else   echo -a $timestamp 14|---› 14Clone:1 ( $+ $numtok(%clone,44) $+ ) user 04/01 %clone
  haltdef
}
raw 312:*: {
  haltdef
  echo -a $timestamp 14|---› 14Server:1 $3 04/01 $4 $5 $6
}
raw 313:*: {
  haltdef
  echo -a $timestamp 14|---› 14Network Info:1 $3 $4 $5 $6 $7 $8 $9
}
raw 314:*: {
  haltdef
  echo -a $timestamp 14¯°--------------------------------------------------------------------------------•  
  echo -a $timestamp 14|---› 14Nick:1 $2
  echo -a $timestamp 14|---› 14FullName:1 $6-
  echo -a $timestamp 14|---› 14Ident@Ip:1 $3 $+ 14 $+ @ $+ 01 $+ $4
  var %clone = $kanalclone($2)
  if (%clone == $null)   echo -a $timestamp 14|---› 14Clones:1  (No Clone)
  else   echo -a $timestamp 14|---› 14Clone:1 ( $+ $numtok(%clone,44) $+ ) user 04/01 %clone
  haltdef
}
raw 317:*: {
  haltdef
  echo -a $timestamp 14|---› 14Connect Time:1 $asctime($4) 04/1 $duration($calc($ctime - $4))
  echo -a $timestamp 14|---› 14Idle:1  $duration($3)
}
raw 318:*: {
  haltdef
  .timer -h 1 200 echo -a $timestamp 14¯°--------------------------------------------------------------------------------•  
}
raw 319:*: {
  haltdef
  echo -a $timestamp 14|---› 14Channels:1 $3-
  echo -a $timestamp 14|---› 14Com Channels:1 $ortak_k($2) 04/01 Total:04 $comchan($2,0) 01Chan(s)
}
raw 338:*: {
  haltdef
  echo -a $timestamp 14|---› 14Real Ip:01 $4 $5 $6
}
raw 320:*: {
  haltdef
  echo -a $timestamp 14|---› 14Swhois:1 $2-
}
raw 330:*: echo -a $timestamp 14|---› 14NickServ:1 $2  $4 $5 $6 $7 $3 | halt
raw 378:*: {
  haltdef
  echo -a $timestamp 14|---› 14Real Ip:01 $3 $4 $5 $6 $7
}
raw 379:*: {
  haltdef
  echo -a $timestamp 14|---› 14Modes:01 $6
}
raw 671:*: {
  haltdef
  echo -a $timestamp 14|---› 14Connection:1 $3-
}
raw 276:*: {
  haltdef
  echo -a $timestamp 14|---› 14Client :1 $3 $4 $5 $6 $7
}
raw 327:*: {
  echo -a $timestamp 14|---› 14Client Info:1 Java User
  halt
}
raw 301:*: {
  haltdef
  echo -a $timestamp 14|---› 14Away:1 ( $+ $3- $+ ) 
}
raw 310:*: {
  haltdef
  echo -a $timestamp 14|---› 14Ircop:1 $3 $4 $5 $6  
}
raw 335:*: { 
  echo -a $timestamp 14|---›  14Bot:1 Yes (+B).  
  halt
}
alias kanalclone {
  if ($ial($address($1,2),0) == 1) return
  var %nickler = 0 | var %clone = $1
  while (%nickler < $ial($address($1,2),0)) { inc %nickler | var %clone = $addtok(%clone,$ial($address($1,2),%nickler).nick, 44) }
  return %clone
}

alias ortak_k {
  if ($server == $null) {   echo -a $timestamp 14|---› 14No such server. }
  elseif ($chan($me) == 0) {   echo -a $timestamp 14|---› 14No such channel. }
  elseif (!$1) {   echo -a $timestamp 14|---› 14No such nick. }
  elseif ($comchan($1,0) == 0) {   echo -a $timestamp 14|---› 14No such nick/channel. }
  else {
    var %oid 1
    set %orka $comchan($1,0)
    unset %knms
    while (%oid <= %orka) {
      set %knms $comchan($1,%oid) $+ $chr(32) $+ %knms
      inc %oid
    }
    return %knms
  }
}

on *:text:!dive*:#: {
  if (!%dive) {
    set -u6 %dive 1
    var %diver = $rand(1,5)
    if (%diver == 1) msg $chan /me $nick Dives into pool $PoolDive 
    if (%diver == 1) msg $chan /me $nick Dives into water but quickly gets eaten by a shark 
    if (%diver == 3) msg $chan /me $nick Dives into lake $LakeDive 
    if (%diver == 4) msg $chan /me $nick Dives into Stream $StreamDive 
    if (%diver == 5) msg $chan /me $nick Dives into river $RiverDive 
  } 
  else msg $chan You've already tried diving for now.
}

alias PoolDive {
  var %PoolDiveRan = $rand(1,5)
  if (%PoolDiveRan == 1) msg $chan /me $nick Hits bottom of pool and knocks self out 
  if (%PoolDiveRan == 1) msg $chan /me $nick Finds a loaf of golden bread 
  if (%PoolDiveRan == 3) msg $chan /me $nick Finds a slice of golden bread
  if (%PoolDiveRan == 4) msg $chan /me $nick Forgets swimming mask and inhales water.
  if (%PoolDiveRan == 5) msg $chan /me $nick Accidentally dives into piranha pool RIP $nick
  return
}
alias LakeDive {
  var %LakeDiveRan = $rand(1,5)
  if (%LakeDiveRan == 1) msg $chan /me $nick Hits a buoy and knocks self out
  if (%LakeDiveRan == 1) msg $chan /me $nick Finds a loaf of golden bread next to sunken raft
  if (%LakeDiveRan == 3) msg $chan /me $nick Finds a slice of golden bread next to school of fish.
  if (%LakeDiveRan == 4) msg $chan /me $nick Forgets swimming mask and inhales water.
  if (%LakeDiveRan == 5) msg $chan /me $nick Accidentally dives close to an alligator. RIP $nick
  return
}
alias StreamDive {
  var %StreamDiveRan = $rand(1,5)
  if (%LakeDiveRan == 1) msg $chan /me $nick Lands in stream and barely gets wet. Nothing was found.
  if (%LakeDiveRan == 1) msg $chan /me $nick Finds a loaf of golden bread in a tackle box
  if (%LakeDiveRan == 3) msg $chan /me $nick Finds a slice of golden bread in the mouth of a turtle
  if (%LakeDiveRan == 4) msg $chan /me $nick Starts drinking the fresh stream water and forgets to find treasure
  if (%LakeDiveRan == 5) msg $chan /me $nick Dives into the mouth of a snapping turtle. RIP $nick
  return
}
alias RiverDive {
  var %RiverDiveRan = $rand(1,5)
  if (%RiverDiveRan == 1) msg $chan /me $nick Lands in a shallow part of the river. Nothing was found
  if (%RiverDiveRan == 1) msg $chan /me $nick Finds a loaf of golden bread under a rotted tree
  if (%RiverDiveRan == 3) msg $chan /me $nick Finds a slice of golden bread in the mouth of a bass
  if (%RiverDiveRan == 4) msg $chan /me $nick Starts drinking water and forgets to purify it first. $nick gets the runs. WutFace
  if (%RiverDiveRan == 5) msg $chan /me $nick Dives into the river and gets bit by a water moccasin.
  return
}
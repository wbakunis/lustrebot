alias -l ht2 {
  if ($version >= 7) || ($1 <= 255) { 
    returnex $chr($1)
  }
  else {
    if ($1 >= 256) && ($1 < 2048) returnex $chr($calc(192 + $int($calc($1 / 64)))) $+ $chr($calc(128 + ($1 % 64)))
    elseif ($1 >= 2048) && ($1 < 65536) returnex $+($chr($calc(224 +  $int($calc($1 / 4096)))),$chr($calc(128 + ($int($calc($1 / 64)) % 64))),$chr($calc(128 + ($1 % 64))))
    elseif ($1 >= 65536) && ($1 < 2097152) returnex $+($chr($calc(240 +  $int($calc($1 / 262144)))),$chr($calc(128 + ($int($calc($1 / 4096)) % 64))),$chr($calc(128 + ($int($calc($1 / 64)) % 64))),$chr($calc(128 + ($1 % 64))))
  }
} 

alias -l html_fix {
  if (!$1) { return }
  if (#x* iswm $1) { 
    return $ht2($base($mid($1,3),16,10))
  }
  if (#* iswm $1) { 
    return $ht2($mid($1,2)) 
  }
  return $1-
}

alias html2ascii { 
  return $regsubex(~, $1, /&(.+);/Ug, $html_fix(\t)) 
}

on $*:text:m@(https?\x2c//|)(www\.|)youtube.com/watch\?v=(.+)@Si:#: {
  if (!$sock(youtube)) {
    sockopen -e youtube www.youtube.com 443
    sockmark youtube $chan $regml(3)
  }
}

on $*:text:m@(https?\x2c//|)(www\.|)youtu.be\(.+)@Si:#: {
  if (!$sock(youtube)) {
    sockopen -e youtube www.youtube.com 443
    sockmark youtube $chan $regml(3)
  }
}

on $*:text:m@(http?\x2c//|)(www\.|)youtube.com/watch\?v=(.+)@Si:#: {
  if (!$sock(youtube)) {
    sockopen -e youtube www.youtube.com 443
    sockmark youtube $chan $regml(3)
  }
}
on $*:text:m@(http?\x2c//|)(www\.|)youtu.be/(.+)@Si:#: {
  if (!$sock(youtube)) {
    sockopen -e youtube www.youtube.com 443
    sockmark youtube $chan $regml(3)
  }
}

on *:sockopen:youtube: {
  sockwrite -n $sockname GET $+(/watch?v=,$gettok($sock(youtube).mark,2,32)) HTTP/1.0
  sockwrite -n $sockname Host: www.youtube.com
  sockwrite -n $sockname
}

on *:sockread:youtube: {
  var %x
  sockread %x
  if (!$sockbr) {
    return
  }
  if ($regex(%x,/<title>(.+)</title>/s)) { 
    msg $gettok($sock(youtube).mark,1,32) $html2ascii($left($regml(1),-9))
  }
}

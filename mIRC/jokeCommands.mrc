;on *:text:!joke:#: {
;  picjoke 
;}
alias picjoke {
  var %u = http://api.icndb.com/jokes/random/
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %u
  var %joke = $JSON(%v,value,joke)
  msg $chan /me %joke
  :end
  JSONClose %v
}

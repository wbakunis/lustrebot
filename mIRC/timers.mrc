on *:text:!em *:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {  
    var %emFile = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\emergencymessage.txt
    if ($exists(%emFile)) {
      ; msg $chan /color blue
      msg $chan /w $nick emergency messaging enabled!
      write %emFile $2- $+
      /timerem 0 500 msg $chan /me $read(%emFile, n) 
      ; !emoff to disable emergency message.
    }
    else msg $chan %gloLostFile
  }
  else msg $chan %gloperm
}

on *:text:!emoff:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {    
    var %emFile = D:\Other Files\Google Drive\Public\projects\twitch\Lustredust\emergencymessage.txt
    if ($exists(%emFile)) {
      ; msg $chan /color hotpink  
      msg $chan /w $nick emergency messaging disabled & file cleared!
      write -c $qt(%emFile)
      /timerem off
    }
    else msg $chan %gloLostFile
  }
  else msg $chan %gloperm
}

on *:text:!mtimer on:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {  
    msg $chan /w $nick Music timers enabled!
    /timer1 0 1300 msg $chan music suggestions are enabled! use .!songrequest <youtube link>
  }
  else msg $chan %gloperm
}

on *:text:!mtimer off:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {       
    msg $chan /w $nick Music timers disabled
    /timer1 off
  }
  else msg $chan %gloperms
}

on *:text:!timers on:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {  
    msg $chan /w $nick General announcement timers enabled!
    /timer1 0 1300 msg $chan social sites. http://www.lmmakeupartist.com/ https://www.instagram.com/lustredust/ https://twitter.com/lustredust https://soundcloud.com/lianne-moseley https://www.facebook.com/lmmakeupartist
    /timer2 0 1500 msg $chan if you have an art idea, use .!suggestart <idea> <picture>
    /timer3 0 1600 msg $chan proudly sponsored by kryolan professional makeup https://global.kryolan.com/ primal contact lenses http://primal-contact-lenses.myshopify.com/ ben nye makeup http://bennye.com/
    /timer4 0 2100 msg $chan check out my youtube channel! https://www.youtube.com/user/lustredust
    /timer5 0 1200 msg $chan to see bot commands, use .!commands
  }
  else msg $chan %gloperm
}

on *:text:!timers off:#: {
  if (($nick isop $chan) || ($nick == colonelpetrenko)) || (($hasmodpowers) || ($isbroadcaster)) {       
    ; msg $chan /w $nick General announcement timers disabled!
    /timer1 off
    /timer2 off
    /timer3 off
    /timer4 off
    /timer5 off
  }
  else msg $chan %gloperms
}

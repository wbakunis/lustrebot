on *:text:!uptime:#: {
  chanonline
}

on *:text:!chuck:#: {
  chuckjoke
}

on *:text:!title:#: {
  chantitle
}

on *:text:!jsver:#: {
  jsonversion
}

;on *:text:!lf:#: {
;  lastfollow
;}

on *:text:!stats:#: {
  chanstats
}

;on *:text:!sched:#: {
;  chansched
;}

alias -l chanonline {
  JSONOpen -uw streamuptime https://api.twitch.tv/kraken/streams/ $+ $1
  JSONHttpHeader streamuptime Client-ID h1qvczn5j1ehp69bfl03ymtvrbfurts
  JSONHttpFetch streamuptime
  VAR %x $IIF($JSON(streamuptime, stream, created_at).value,$duration($calc($ctime - $TwitchTime($JSON(streamuptime, stream, created_at).value)),2),????)
  JSONClose streamuptime
  RETURN %x
}

alias -l TwitchTime {
  if ($regex($1-, /^(\d\d(?:\d\d)?)-(\d\d)-(\d\d)T(\d\d)\:(\d\d)\:(\d\d)(?:(?:Z$)|(?:([+-])(\d\d)\:(\d+)))?$/i)) {
    var %m = $Gettok(January February March April May June July August September October November December, $regml(2), 32), %d = $ord($base($regml(3),10,10)), %o = +0, %t
    if ($regml(0) > 6) %o = $regml(7) $+ $calc($regml(8) * 3600 + $regml(9))
    %t = $calc($ctime(%m %d $regml(1) $regml(4) $+ : $+ $regml(5) $+ : $+ $regml(6)) - %o)
    if ($asctime(zz) !== 0 && $regex($v1, /^([+-])(\d\d)(\d+)$/)) {
      %o = $regml(1) $+ $calc($regml(2) * 3600 + $regml(3))
      %t = $calc(%t + %o )
    }
    return %t
  }
}

alias chantitle {
  var %u = https://api.twitch.tv/kraken/channels/lustredust?client_id=h1qvczn5j1ehp69bfl03ymtvrbfurts
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %u
  var %id = $JSON(%v,stream,_id)
  var %status = $JSON(%v,status)
  msg $chan %status 
  :end
  JSONClose %v
}

alias chanstats {
  var %u = https://api.twitch.tv/kraken/channels/lustredust?client_id=h1qvczn5j1ehp69bfl03ymtvrbfurts
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %u
  var %cfoll = $JSON(%v,followers)
  var %cview = $JSON(%v,views)
  msg $chan channel followers: %cfoll & channel views %cview
  :end
  JSONClose %v
}

alias chansched {
  var %u = https://api.twitch.tv/api/channels/lustredust/panels?client_id=h1qvczn5j1ehp69bfl03ymtvrbfurts
  var %v = twc_ $+ $ticks
  JSONOpen -ud %v %u
  var %sched = $JSON(%v,//html_description)
  msg $chan schedule: %sched
  :end
  JSONClose %v
}

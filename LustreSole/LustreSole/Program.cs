﻿using System;
using System.Reflection;
using System.IO;
using LustreSole.Scripts;

namespace LustreSole
{
    public class Program
    {

        static void Main(string[] args)
        {
            var ls = new Startup();
            ls.StartBot();
        }
    }
}
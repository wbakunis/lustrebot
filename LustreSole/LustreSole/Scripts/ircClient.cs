﻿using System;
using System.IO;
using System.Net.Sockets;

namespace LustreSole.Scripts
{
    public class ircClient
    {
        public string channel;
        public int dividedSemiColonPos;
        public int dividedSpacePos;

        //private string TextPrimer = "#";
        public string iRCMessage;

        public bool isConnected;
        public bool isMod;
        public bool isSubscriber;
        public bool isTurbo;
        public Startup lirc;
        public NetworkStream netStream;
        public int port = 6667;
        public StreamReader reader;
        public string SubSpaceString;
        public string SubString;
        public TcpClient tcpClient;
        public int TextColor;
        public string url = "irc.chat.twitch.tv";
        public string user;
        public StreamWriter writer;

        public ircClient(string userName, string password)
        {
            this.user = userName;
            tcpClient = new TcpClient(url, port);
            reader = new StreamReader(tcpClient.GetStream());
            writer = new StreamWriter(tcpClient.GetStream());

            writer.WriteLine("PASS " + password);
            writer.WriteLine("NICK " + userName);
            writer.WriteLine("USER " + userName + " 8 * :" + userName);
            writer.Flush();
        }

        public void ClearChat()
        {
            postMessage(":tmi.twitch.tv CLEARCHAT #lustredust");
        }

        public void onJoinRoom(string channel)
        {
            this.channel = channel;
            writer.WriteLine("JOIN " + channel);
            writer.WriteLine("CAP REQ :twitch.tv/membership");
            writer.WriteLine("CAP REQ :twitch.tv/commands");
            writer.WriteLine("CAP REQ :twitch.tv/tags");

            if (tcpClient.Connected)
            {
                Console.WriteLine("Connected! " + tcpClient.Client.RemoteEndPoint);
                isConnected = true;
            }
            writer.Flush();
        }

        public void postMessage(string message)
        {
            writer.WriteLine(message);
            writer.Flush();
        }

        public string readMessage()
        {
            iRCMessage = reader.ReadLine();
            if (iRCMessage == null)
            {
                iRCMessage = "";
            }
            Console.WriteLine(iRCMessage);
            return iRCMessage;
        }

        public void sendMessage(string message)
        {
            postMessage(":" + user + "!" + user + "@" + user + ".tmi.twitch.tv PRIVMSG #lustredust :" + message);
            writer.Flush();
        }

        public void splitMessage()
        {
            String[] split_SemiColon = iRCMessage.Split(';');
            if (split_SemiColon.Length <= 1)
            {
                return;
            }
            else
            {
                if (split_SemiColon.Length > 1)
                {
                    foreach (string dividedString in split_SemiColon)
                    {
                        dividedSemiColonPos = Array.IndexOf(split_SemiColon, dividedString);
                        SubString = dividedString.Substring(dividedString.LastIndexOf("=", StringComparison.Ordinal) + 1);
                        //Console.WriteLine(dividedPos + "|" + SubString);
                        return;
                    }
                }
                //Console.WriteLine(split_iRCMessage[4]);
                //Console.WriteLine(split_iRCMessage[4].Substring(split_iRCMessage[4].LastIndexOf('=')));
                if (split_SemiColon[4].Substring(split_SemiColon[4].LastIndexOf('=') + 1) == "1")
                {
                    isMod = true;
                    this.sendMessage("Is a mod!");
                    return;
                }
                else
                {
                    isMod = false;
                    return;
                }
            }
        }
    }
}
﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Timers;

namespace LustreSole.Scripts.JSON_Handlers
{
    public class WunderGround_JSON
    {
        private string WunderGroundAPi_loc = "D:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/private/wundergroundAPi.txt";
        private string WunderGroundAPi_url;
        private string WunderGroundJSON_loc = "D:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/Lustrebot/LustreSole/LustreSole/JSON Files/WunderGround.json";
        //private System.Timers.Timer WunderGroundTimer;
        //private int cTimer_time = 60000;//1 minute | 1000ms = 1second

        //public void onRetrieveWunderGroundTimer() {
        //    WunderGroundTimer = new System.Timers.Timer(cTimer_time);
        //    WunderGroundTimer.Elapsed += onRetrieveWunderGroundInfo;
        //    Console.WriteLine("WunderGround Timers starting! | " + DateTime.Now.ToLongDateString());
        //    WunderGroundTimer.AutoReset = true;
        //    WunderGroundTimer.Enabled = true;
        //}

        public void onGetWunderGroundAPi()
        {
            try
            {
                using (StreamReader sr = new StreamReader(WunderGroundAPi_loc))
                {
                    WunderGroundAPi_url = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("WunderGroundAPi File not found");
                Console.WriteLine(e);
            }
        }

        public void onRetrieveWunderGroundInfo(Object o, ElapsedEventArgs eea)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    Console.WriteLine("[UPDATING WUNDERGROUND.JSON FILE!] | " + DateTime.UtcNow);
                    UnicodeEncoding byter = new UnicodeEncoding();
                    var retrievedJSON = wc.DownloadString(WunderGroundAPi_url);
                    string formatJSON = JValue.Parse(retrievedJSON).ToString(Formatting.Indented);
                    System.IO.File.WriteAllText(WunderGroundJSON_loc, formatJSON);
                    //Properties.Resources.Chatters;
                    //Console.Write(json);
                    // Now parse with JSON.Net
                }
            }
            catch (WebException we)
            {
                Console.WriteLine(we);
            }
        }
    }
}
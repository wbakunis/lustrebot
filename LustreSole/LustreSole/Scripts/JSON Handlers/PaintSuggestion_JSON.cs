﻿namespace LustreSole.Scripts
{
    internal class PaintSuggestion_JSON
    {
        public string ChatterName_suggest { get; set; }
        public string ChatterSuggestion { get; set; }
        public string ChatterSuggestionDate { get; set; }
        public string ChatterSuggestionURL { get; set; }
    }
}
﻿namespace LustreSole.Scripts.JSON_Handlers
{
    public class ToastManager_JSON
    {
        public ircClient client;

        private ToastManager_JSON toast = new ToastManager_JSON
        {
        };

        public string ChatterName_toast { get; set; }
        public int ChatterToast_count { get; set; }
    }
}
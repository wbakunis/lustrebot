﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Text;
using System.Timers;

namespace LustreSole.Scripts.JSON_Handlers
{
    public class Followers_JSON
    {
        public string LastFollower_loc = "D:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/Lustrebot/LustreSole/LustreSole/JSON Files/LastFollower.json";
        public string LastFollower_url = "https://api.twitch.tv/kraken/channels/lustredust/follows?limit=1";
        //private System.Timers.Timer LastFollowerTimer;
        //private int cTimer_time = 60000;//1 minute | 1000ms = 1second

        //public void onRetrieveLFTimer() {
        //    LastFollowerTimer = new System.Timers.Timer(cTimer_time);
        //    LastFollowerTimer.Elapsed += onRetrieveLastFollowerInfo;
        //    Console.WriteLine("LastFollower Timers starting! | " + DateTime.Now.ToLongDateString());
        //    LastFollowerTimer.AutoReset = true;
        //    LastFollowerTimer.Enabled = true;
        //}

        public void onRetrieveLastFollowerInfo(Object o, ElapsedEventArgs eea)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    Console.WriteLine("[UPDATING LASTFOLLOWER.JSON FILE!] | " + DateTime.UtcNow);
                    UnicodeEncoding byter = new UnicodeEncoding();
                    var retrievedJSON = wc.DownloadString(LastFollower_url);
                    string formatJSON = JValue.Parse(retrievedJSON).ToString(Formatting.Indented);
                    System.IO.File.WriteAllText(LastFollower_loc, formatJSON);
                    //Properties.Resources.Chatters;
                    //Console.Write(formatJSON);
                    // Now parse with JSON.Net
                }
            }
            catch (WebException we)
            {
                Console.WriteLine(we);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LustreSole.Scripts;
using LustreSole.Scripts.JSON_Handlers;
using LustreSole.Scripts.Mechanics;
using System.Reflection;
using System.IO;
using System.Diagnostics;

namespace LustreSole.Scripts
{
    public class Startup
    {

        Assembly rA = Assembly.GetExecutingAssembly();
        AssemblyName name;

        public int ChatLineCount;

        public string Chatters;

        //public ChannelMisc_JSON cm;
        public string fileLoc = "D:/Other Files/Google Drive/Public/projects/twitch/Lustredust Channel/private/LustreReader/LustreReader_oauth.txt";

        public Followers_JSON fj;
        public ircClient irc;

        //public Chatters_JSON cj;
        public JSON_timer jt;

        public ParseViewers pv;

        //public ModCommands mc;
        public string reader;

        public string RMSplit;
        public string UserMessage;
        public string UserInfo;
        public int ViewerCount;

        public bool isConnected;

        public void StartBot()
        {
            name = rA.GetName();
            Console.Title = name.Name + " | " + name.Version;
            Console.ForegroundColor = ConsoleColor.Green;
            AuthorizeForm();
            jt = new JSON_timer();
            pv = new ParseViewers();
            pv.onParseViewersTimer();
            jt.onStartJSONTimers();
            onBotConnect();
        }

        private void AuthorizeForm()
        {
            try
            {
                using (StreamReader sr = new StreamReader(fileLoc))
                {
                    reader = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[AUTHORIZATION] oAuth File not found");
                Console.ReadKey();
            }
        }

        public void onBotConnect()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            irc = new ircClient("Lustrebot", reader);
            irc.onJoinRoom("#lustredust");
            sw.Stop();
            Console.WriteLine("LustreSole connected in " + sw.ElapsedMilliseconds + " MS");
            //irc.sendMessage("LustreSole connected in " + sw.ElapsedMilliseconds + " MS");
            while (true)
            {
                isConnected = true;
                int bufferSize = irc.tcpClient.ReceiveBufferSize;
                int aData = irc.tcpClient.Available;
                //string Msg;
                //Console.WriteLine(bufferSize);
                //Console.WriteLine(aData);
                UserMessage = irc.readMessage();
                StringSplitter();
                if (UserMessage.Contains("!lbconsole"))
                {
                    irc.sendMessage("Console is running. | version: " + name.Version);
                    //irc_TextBox.Text = "LustreReader is active" + "\n";
                }
                if (UserMessage.Contains("!Clear"))
                {
                    irc.ClearChat();
                }
                if (UserMessage.Contains("youtube.com"))
                {

                }
                //if (message.Contains("!tr")) {
                //    irc.sendMessage("LustreReader is active");
                //    //irc_TextBox.Text = "LustreReader is active" + "\n";
                //}
                //if (message.Contains("!tr disable")) {
                //    Application.Exit();
                //    //irc_TextBox.Text = "LustreReader is active" + "\n";
                //}
            }
        }
        public void StringSplitter()
        {
            if (isConnected)
            {
                //UserMessage = irc.readMessage();
                UserInfo = irc.readMessage();
                string[] umSplit = UserMessage.Split(':');
                string[] uiSplit = UserInfo.Split(';');
                foreach (String InfoSplit in uiSplit)
                {
                    string SubInfo = InfoSplit.Split(new string[] { "=" }, StringSplitOptions.None).Last();
                    Console.WriteLine("InfoObj | " + SubInfo);
                }
                RMSplit = irc.readMessage().Split(new string[] { ":" }, StringSplitOptions.None).Last();
                //Console.WriteLine("RMSplit |" + RMSplit);
                Console.WriteLine(RawMessage());
            }
        }

        public string RawMessage()
        {
            return RMSplit;
        }
    }
}